/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import static ru.enterix.nettysmpplib.pdus.ByteData.getCString;
import static ru.enterix.nettysmpplib.pdus.ByteData.validationCString;

/**
 *
 * @author SoftProfi
 */
public class AddressRange extends ByteData {

    public static final int MAX_LEN_BYTES = SmLenField.SM_ADDR_RANGE_LEN + 2;
    private byte ton;
    private byte npi;
    private String address;

    @Override
    public ChannelBuffer getBytesFromData() {
        ChannelBuffer bufAddress = ChannelBuffers.buffer(MAX_LEN_BYTES);
        bufAddress.writeByte(ton);
        bufAddress.writeByte(npi);
        writeToChannelBufferFromCString(bufAddress, address);
        return bufAddress;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {

        ton = buffer.readByte();
        npi = buffer.readByte();
        setAddress(getCString(buffer));

    }

    public void set(byte ton, byte npi, String addr) {
        this.ton = ton;
        this.npi = npi;
        setAddress(addr);
    }

    public void setTon(byte ton) {
        this.ton = ton;
    }

    public void setNpi(byte npi) {
        this.npi = npi;
    }

    public void setAddress(String address) {
        validationCString(SmLenField.SM_ADDR_RANGE_LEN, address);
        this.address = address;
    }

    public byte getTon() {
        return ton;
    }

    public byte getNpi() {
        return npi;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        StringBuilder debugString = new StringBuilder();
        debugString
                .append(" ton= ").append(ton)
                .append(" npi= ").append(npi)
                .append(" address= ").append(address);

        return debugString.toString();

    }

}
