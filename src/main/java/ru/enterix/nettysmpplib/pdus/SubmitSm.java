/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import java.util.ArrayList;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.EncodeString;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_ALERT_ON_MSG_DELIVERY;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_ATAG;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_ATAG_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_ATAG_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_PRES_IND;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DISPLAY_TIME;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DST_ADDR_SUBUNIT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DST_PORT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_ITS_REPLY_TYPE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_ITS_SESSION_INFO;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_LANG_IND;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MORE_MSGS;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_WAIT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MS_VALIDITY;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_NUM_MSGS;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_PAYLOAD_TYPE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_PRIV_IND;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_MSG_REF_NUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_SEG_SNUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_TOT_SEG;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SMS_SIGNAL;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_ADDR_SUBUNIT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_PORT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_USER_MSG_REF;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_USER_RESP_CODE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_USSD_SER_OP;
import ru.enterix.nettysmpplib.constants.SmLenField;
import static ru.enterix.nettysmpplib.constants.SmLenField.SM_DATE_LEN;
import ru.enterix.nettysmpplib.exceptions.InvalidLengthOptinalParametrException;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import ru.enterix.nettysmpplib.exceptions.ValueNotSetException;
import static ru.enterix.nettysmpplib.pdus.ByteData.getCString;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVByte;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVEmpty;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVOctets;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVShort;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVUByte;

/**
 *
 * @author SoftProfi
 */
public class SubmitSm extends Request {

    private static int FIXED_LEN_BYTES = 8;
    private static final int COUNT_OPTIMAL_PARAM = 28;

    //<editor-fold defaultstate="collapsed" desc="Main Fields">
    private String serviceType;
    private Address sourceAddress = new Address();
    private Address destAddress = new Address();
    private byte esmClass;
    private byte protocolId;
    private byte priorityFlag;
    private String scheduleDeliveryTime;
    private String validityPeriod;
    private byte registeredDelivery;
    private byte replaceIfPresentFlag;
    private byte dataCoding = EncodeString.LATIN1_int;
    private byte smDefaultMsgId;
    private short smLength;
    private String shortMessage = "";
   // private ShortMessage shortMessage = new ShortMessage(SM_MSG_LEN);
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Others Fields">
    private TLVShort userMessageReference = new TLVShort(OPT_PAR_USER_MSG_REF);
    private TLVShort sourcePort = new TLVShort(OPT_PAR_SRC_PORT);
    private TLVByte sourceAddrSubunit = new TLVByte(OPT_PAR_SRC_ADDR_SUBUNIT);
    private TLVShort destinationPort = new TLVShort(OPT_PAR_DST_PORT);
    private TLVByte destAddrSubunit = new TLVByte(OPT_PAR_DST_ADDR_SUBUNIT);
    private TLVShort sarMsgRefNum = new TLVShort(OPT_PAR_SAR_MSG_REF_NUM);
    private TLVUByte sarTotalSegments = new TLVUByte(OPT_PAR_SAR_TOT_SEG);
    private TLVUByte sarSegmentSeqnum = new TLVUByte(OPT_PAR_SAR_SEG_SNUM);
    private TLVByte moreMsgsToSend = new TLVByte(OPT_PAR_MORE_MSGS);
    private TLVByte payloadType = new TLVByte(OPT_PAR_PAYLOAD_TYPE);
    private TLVOctets messagePayload = new TLVOctets(OPT_PAR_MSG_PAYLOAD, OPT_PAR_MSG_PAYLOAD_MIN, OPT_PAR_MSG_PAYLOAD_MAX);
    private TLVByte privacyIndicator = new TLVByte(OPT_PAR_PRIV_IND);
    private TLVOctets callbackNum = new TLVOctets(OPT_PAR_CALLBACK_NUM, OPT_PAR_CALLBACK_NUM_MIN, OPT_PAR_CALLBACK_NUM_MAX); // 4-19
    private TLVByte callbackNumPresInd = new TLVByte(OPT_PAR_CALLBACK_NUM_PRES_IND);
    private TLVOctets callbackNumAtag = new TLVOctets(OPT_PAR_CALLBACK_NUM_ATAG, OPT_PAR_CALLBACK_NUM_ATAG_MIN,
            OPT_PAR_CALLBACK_NUM_ATAG_MAX);
    private TLVOctets sourceSubaddress = new TLVOctets(OPT_PAR_SRC_SUBADDR, OPT_PAR_SRC_SUBADDR_MIN, OPT_PAR_SRC_SUBADDR_MAX); // 2-23 
    private TLVOctets destSubaddress = new TLVOctets(OPT_PAR_DEST_SUBADDR, OPT_PAR_DEST_SUBADDR_MIN, OPT_PAR_DEST_SUBADDR_MAX);
    private TLVByte userResponseCode = new TLVByte(OPT_PAR_USER_RESP_CODE);
    private TLVByte displayTime = new TLVByte(OPT_PAR_DISPLAY_TIME);
    private TLVShort smsSignal = new TLVShort(OPT_PAR_SMS_SIGNAL);
    private TLVByte msValidity = new TLVByte(OPT_PAR_MS_VALIDITY);
    private TLVByte msMsgWaitFacilities = new TLVByte(OPT_PAR_MSG_WAIT); // bit mask
    private TLVByte numberOfMessages = new TLVByte(OPT_PAR_NUM_MSGS);
    private TLVEmpty alertOnMsgDelivery = new TLVEmpty(OPT_PAR_ALERT_ON_MSG_DELIVERY);
    private TLVByte languageIndicator = new TLVByte(OPT_PAR_LANG_IND);
    private TLVByte itsReplyType = new TLVByte(OPT_PAR_ITS_REPLY_TYPE);
    private TLVShort itsSessionInfo = new TLVShort(OPT_PAR_ITS_SESSION_INFO);
    private TLVByte ussdServiceOp = new TLVByte(OPT_PAR_USSD_SER_OP);
    //</editor-fold>

    public SubmitSm() {
        super(CommandId.SUBMIT_SM);
        ArrayList reg = new ArrayList(COUNT_OPTIMAL_PARAM);
        reg.add(userMessageReference);
        reg.add(sourcePort);
        reg.add(sourceAddrSubunit);
        reg.add(destinationPort);
        reg.add(destAddrSubunit);
        reg.add(sarMsgRefNum);
        reg.add(sarTotalSegments);
        reg.add(sarSegmentSeqnum);
        reg.add(moreMsgsToSend);
        reg.add(payloadType);
        reg.add(messagePayload);
        reg.add(privacyIndicator);
        reg.add(callbackNum);
        reg.add(callbackNumPresInd);
        reg.add(callbackNumAtag);
        reg.add(sourceSubaddress);
        reg.add(destSubaddress);
        reg.add(userResponseCode);
        reg.add(displayTime);
        reg.add(smsSignal);
        reg.add(msValidity);
        reg.add(msMsgWaitFacilities);
        reg.add(numberOfMessages);
        reg.add(alertOnMsgDelivery);
        reg.add(languageIndicator);
        reg.add(itsReplyType);
        reg.add(itsSessionInfo);
        reg.add(ussdServiceOp);
        registerOptionalParam(reg);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        ChannelBuffer sourceAddressBytes = sourceAddress.getBytesFromData();
        ChannelBuffer destAddressBytes = destAddress.getBytesFromData();
        int maxLenBytes = (FIXED_LEN_BYTES + lenCString(serviceType) + sourceAddressBytes.readableBytes()
                + destAddressBytes.readableBytes() + lenCString(scheduleDeliveryTime) + lenCString(validityPeriod) + smLength);
        ChannelBuffer buff = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(buff, serviceType);
        buff.writeBytes(sourceAddressBytes);
        buff.writeBytes(destAddressBytes);
        buff.writeByte(esmClass);
        buff.writeByte(protocolId);
        buff.writeByte(priorityFlag);
        writeToChannelBufferFromCString(buff, scheduleDeliveryTime);
        writeToChannelBufferFromCString(buff, validityPeriod);
        buff.writeByte(registeredDelivery);
        buff.writeByte(replaceIfPresentFlag);
        buff.writeByte(dataCoding);
        buff.writeByte(smDefaultMsgId);
        buff.writeByte(encodeUnsigned(smLength));
        buff.writeBytes(shortMessage.getBytes(EncodeString.getCharSet(dataCoding)));
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setServiceType(getCString(buffer));
        sourceAddress.setDataFromBytes(buffer);
        destAddress.setDataFromBytes(buffer);
        esmClass = buffer.readByte();
        protocolId = buffer.readByte();
        priorityFlag = buffer.readByte();
        setScheduleDeliveryTime(getCString(buffer));
        setValidityPeriod(getCString(buffer));
        registeredDelivery = buffer.readByte();
        replaceIfPresentFlag = buffer.readByte();
        dataCoding = buffer.readByte();
        smDefaultMsgId = buffer.readByte();
        smLength = buffer.readUnsignedByte();
        ChannelBuffer readBytes = buffer.readBytes(smLength);
        shortMessage = new String(readBytes.array(), EncodeString.getCharSet(dataCoding));

    }

    @Override
    public Response createResponse() {
        return new SubmitSmResp();
    }

    //<editor-fold defaultstate="collapsed" desc="Has">
    public boolean hasUserMessageReference() {
        return userMessageReference.hasValue();
    }

    public boolean hasSourcePort() {
        return sourcePort.hasValue();
    }

    public boolean hasSourceAddrSubunit() {
        return sourceAddrSubunit.hasValue();
    }

    public boolean hasDestinationPort() {
        return destinationPort.hasValue();
    }

    public boolean hasDestAddrSubunit() {
        return destAddrSubunit.hasValue();
    }

    public boolean hasSarMsgRefNum() {
        return sarMsgRefNum.hasValue();
    }

    public boolean hasSarTotalSegments() {
        return sarTotalSegments.hasValue();
    }

    public boolean hasSarSegmentSeqnum() {
        return sarSegmentSeqnum.hasValue();
    }

    public boolean hasMoreMsgsToSend() {
        return moreMsgsToSend.hasValue();
    }

    public boolean hasPayloadType() {
        return payloadType.hasValue();
    }

    public boolean hasMessagePayload() {
        return messagePayload.hasValue();
    }

    public boolean hasPrivacyIndicator() {
        return privacyIndicator.hasValue();
    }

    public boolean hasCallbackNum() {
        return callbackNum.hasValue();
    }

    public boolean hasCallbackNumPresInd() {
        return callbackNumPresInd.hasValue();
    }

    public boolean hasCallbackNumAtag() {
        return callbackNumAtag.hasValue();
    }

    public boolean hasSourceSubaddress() {
        return sourceSubaddress.hasValue();
    }

    public boolean hasDestSubaddress() {
        return destSubaddress.hasValue();
    }

    public boolean hasUserResponseCode() {
        return userResponseCode.hasValue();
    }

    public boolean hasDisplayTime() {
        return displayTime.hasValue();
    }

    public boolean hasSmsSignal() {
        return smsSignal.hasValue();
    }

    public boolean hasMsValidity() {
        return msValidity.hasValue();
    }

    public boolean hasMsMsgWaitFacilities() {
        return msMsgWaitFacilities.hasValue();
    }

    public boolean hasNumberOfMessages() {
        return numberOfMessages.hasValue();
    }

    public boolean hasAlertOnMsgDelivery() {
        return alertOnMsgDelivery.hasValue();
    }

    public boolean hasLanguageIndicator() {
        return languageIndicator.hasValue();
    }

    public boolean hasItsReplyType() {
        return itsReplyType.hasValue();
    }

    public boolean hasItsSessionInfo() {
        return itsSessionInfo.hasValue();
    }

    public boolean hasUssdServiceOp() {
        return ussdServiceOp.hasValue();
    }
  //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Main Setters">
    public void setServiceType(String value) {
        validationCString(SmLenField.SM_SRVTYPE_LEN, value);
        this.serviceType = value;
    }

    public void setSourceAddress(byte ton, byte npi, String address) {
        sourceAddress.set(ton, npi, address);
    }

    public void setDestAddress(byte ton, byte npi, String address) {
        destAddress.set(ton, npi, address);
    }

    public void setEsmClass(byte value) {
        esmClass = value;
    }

    public void setProtocolId(byte value) {
        protocolId = value;
    }

    public void setPriorityFlag(byte value) {
        priorityFlag = value;
    }

    public void setScheduleDeliveryTime(String value) {
        validationCString(SM_DATE_LEN, value);
        scheduleDeliveryTime = value;
    }

    public void setValidityPeriod(String value) {
        validityPeriod = value;
        validationCString(SM_DATE_LEN, value);
    }

    public void setRegisteredDelivery(byte value) {
        registeredDelivery = value;
    }

    public void setReplaceIfPresentFlag(byte value) {
        replaceIfPresentFlag = value;
    }

    public void setDataCoding(byte encode) {
        dataCoding = encode;
    }

    public void setSmDefaultMsgId(byte value) {
        smDefaultMsgId = value;
    }

    /*private void setSmLength(short smLength) {
     shortMessage.setLength = smLength;
     }*/
    public void setShortMessage(String message) {
        byte[] temp = message.getBytes(EncodeString.getCharSet(dataCoding));
        validationString(0, SmLenField.SM_MSG_LEN, temp.length);
        shortMessage = message;
        smLength = (short) temp.length;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Others Setters">
    public void setUserMessageReference(short value) {
        userMessageReference.setValue(value);
    }

    public void setSourcePort(short value) {
        sourcePort.setValue(value);
    }

    public void setSourceAddrSubunit(byte value) {
        sourceAddrSubunit.setValue(value);
    }

    public void setDestinationPort(short value) {
        destinationPort.setValue(value);
    }

    public void setDestAddrSubunit(byte value) {
        destAddrSubunit.setValue(value);
    }

    public void setSarMsgRefNum(short value) {
        sarMsgRefNum.setValue(value);
    }

    public void setSarTotalSegments(short value) {
        sarTotalSegments.setValue(value);
    }

    public void setSarSegmentSeqnum(short value) {
        sarSegmentSeqnum.setValue(value);
    }

    public void setMoreMsgsToSend(byte value) {
        moreMsgsToSend.setValue(value);
    }

    public void setPayloadType(byte value) {
        payloadType.setValue(value);
    }

    public void setMessagePayload(byte[] value)
            throws InvalidLengthOptinalParametrException {
        messagePayload.setValue(value);
    }

    public void setPrivacyIndicator(byte value) {
        privacyIndicator.setValue(value);
    }

    public void setCallbackNum(byte[] value)
            throws InvalidLengthOptinalParametrException {
        callbackNum.setValue(value);
    }

    public void setCallbackNumPresInd(byte value) {
        callbackNumPresInd.setValue(value);
    }

    public void setCallbackNumAtag(byte[] value)
            throws InvalidLengthOptinalParametrException {
        callbackNumAtag.setValue(value);
    }

    public void setSourceSubaddress(byte[] value)
            throws InvalidLengthOptinalParametrException {
        sourceSubaddress.setValue(value);
    }

    public void setDestSubaddress(byte[] value)
            throws InvalidLengthOptinalParametrException {
        destSubaddress.setValue(value);
    }

    public void setUserResponseCode(byte value) {
        userResponseCode.setValue(value);
    }

    public void setDisplayTime(byte value) {
        displayTime.setValue(value);
    }

    public void setSmsSignal(short value) {
        smsSignal.setValue(value);
    }

    public void setMsValidity(byte value) {
        msValidity.setValue(value);
    }

    public void setMsMsgWaitFacilities(byte value) {
        msMsgWaitFacilities.setValue(value);
    }

    public void setNumberOfMessages(byte value) {
        numberOfMessages.setValue(value);
    }

    public void setAlertOnMsgDelivery(boolean value) {
        alertOnMsgDelivery.setValue(value);
    }

    public void setLanguageIndicator(byte value) {
        languageIndicator.setValue(value);
    }

    public void setItsReplyType(byte value) {
        itsReplyType.setValue(value);
    }

    public void setItsSessionInfo(short value) {
        itsSessionInfo.setValue(value);
    }

    public void setUssdServiceOp(byte value) {
        ussdServiceOp.setValue(value);
    }
  //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Main geters">
    public String getServiceType() {
        return serviceType;
    }

    public Address getSourceAddress() {
        return sourceAddress;
    }

    public Address getDestAddress() {
        return destAddress;
    }

    public byte getEsmClass() {
        return esmClass;
    }

    public byte getProtocolId() {
        return protocolId;
    }

    public byte getPriorityFlag() {
        return priorityFlag;
    }

    public String getScheduleDeliveryTime() {
        return scheduleDeliveryTime;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public byte getRegisteredDelivery() {
        return registeredDelivery;
    }

    public byte getReplaceIfPresentFlag() {
        return replaceIfPresentFlag;
    }

    public byte getDataCoding() {
        return dataCoding;
    }

    public byte getSmDefaultMsgId() {
        return smDefaultMsgId;
    }

    public short getSmLength() {
        return smLength;
    }

    public String getShortMessage() {
        return shortMessage;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Others geters">
    public short getUserMessageReference() {
        return userMessageReference.getValue();
    }

    public short getSourcePort() {
        return sourcePort.getValue();
    }

    public byte getSourceAddrSubunit() {
        return sourceAddrSubunit.getValue();
    }

    public short getDestinationPort() {
        return destinationPort.getValue();
    }

    public byte getDestAddrSubunit() {
        return destAddrSubunit.getValue();
    }

    public short getSarMsgRefNum() {
        return sarMsgRefNum.getValue();
    }

    public short getSarTotalSegments() {
        return sarTotalSegments.getValue();
    }

    public short getSarSegmentSeqnum() {
        return sarSegmentSeqnum.getValue();
    }

    public byte getMoreMsgsToSend() {
        return moreMsgsToSend.getValue();
    }

    public byte getPayloadType() {
        return payloadType.getValue();
    }

    public ChannelBuffer getMessagePayload() {
        return messagePayload.getValue();
    }

    public byte getPrivacyIndicator() {
        return privacyIndicator.getValue();
    }

    public ChannelBuffer callbackNum() {
        return callbackNum.getValue();
    }

    public byte getCallbackNumPresInd() {
        return callbackNumPresInd.getValue();
    }

    public ChannelBuffer getCallbackNumAtag()
            throws ValueNotSetException {
        return callbackNumAtag.getValue();
    }

    public ChannelBuffer getSourceSubaddress() {
        return sourceSubaddress.getValue();
    }

    public ChannelBuffer getDestSubaddress() {
        return destSubaddress.getValue();
    }

    public byte getUserResponseCode() {
        return userResponseCode.getValue();
    }

    public byte getDisplayTime() {
        return displayTime.getValue();
    }

    public short getSmsSignal() {
        return smsSignal.getValue();
    }

    public byte getMsValidity() {
        return msValidity.getValue();
    }

    public byte getMsMsgWaitFacilities() {
        return msMsgWaitFacilities.getValue();
    }

    public byte getNumberOfMessages() {
        return numberOfMessages.getValue();
    }

    public boolean getAlertOnMsgDelivery() {
        return alertOnMsgDelivery.getValue();
    }

    public byte getLanguageIndicator() {
        return languageIndicator.getValue();
    }

    public byte getItsReplyType() {
        return itsReplyType.getValue();
    }

    public short getItsSessionInfo() {
        return itsSessionInfo.getValue();
    }

    public byte getUssdServiceOp() {
        return ussdServiceOp.getValue();
    }
    //</editor-fold>

    @Override
    public String bodyToString() {
        StringBuilder debugString = new StringBuilder();
        debugString
                .append(" Body field-> ")
                .append(" serviceType= ").append(serviceType)
                .append(" sourse adres-> ").append(sourceAddress.toString())
                .append(" destAddress-> ").append(destAddress.toString())
                .append(" esmClass= ").append(esmClass)
                .append(" priorityFlag= ").append(priorityFlag)
                .append(" scheduleDeliveryTime= ").append(scheduleDeliveryTime)
                .append(" validityPeriod= ").append(validityPeriod)
                .append(" registeredDelivery= ").append(registeredDelivery)
                .append(" replaceIfPresentFlag= ").append(replaceIfPresentFlag)
                .append(" dataCoding= ").append(dataCoding)
                .append(" smDefaultMsgId= ").append(smDefaultMsgId)
                .append(" smLength= ").append(shortMessage)
                .append(" shortMessage= ").append(shortMessage);

        return debugString.toString();
    }

}
