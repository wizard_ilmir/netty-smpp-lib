/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import ru.enterix.nettysmpplib.constants.CommandId;

/**
 *
 * @author SoftProfi
 */
public class BindTransmitterResp extends BindResponse{

    public BindTransmitterResp() {
	super(CommandId.BIND_TRANSMITTER_RESP);
    }
    
}
