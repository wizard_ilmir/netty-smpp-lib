/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import java.util.ArrayList;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.EncodeString;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_ALERT_ON_MSG_DELIVERY;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_ATAG;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_ATAG_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_ATAG_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_PRES_IND;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DISPLAY_TIME;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DST_ADDR_SUBUNIT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DST_PORT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_LANG_IND;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_WAIT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MS_VALIDITY;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_PAYLOAD_TYPE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_PRIV_IND;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_MSG_REF_NUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_SEG_SNUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_TOT_SEG;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SMS_SIGNAL;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_ADDR_SUBUNIT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_PORT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_USER_MSG_REF;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.IntegerOutOfRangeException;
import ru.enterix.nettysmpplib.exceptions.InvalidLengthOptinalParametrException;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import static ru.enterix.nettysmpplib.pdus.ByteData.getCString;
import ru.enterix.nettysmpplib.pdus.tlvs.TLV;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVByte;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVEmpty;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVOctets;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVShort;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVUByte;

/**
 *
 * @author SoftProfi
 */
public class SubmitMulti extends Request {

    public static final int MAX_LEN_BYTES = SmLenField.SM_SRVTYPE_LEN + Address.MAX_LEN_BYTES_DEFAULT + 9
            + SmLenField.SM_DATE_LEN + SmLenField.SM_DATE_LEN + SmLenField.SM_MSG_LEN;

    private static final int COUNT_OPTIMAl_PARAM = 22;
    //<editor-fold defaultstate="collapsed" desc="Main Fields">

    private String serviceType;
    private Address sourceAddress = new Address();
    private byte numberOfDests;
    //dest_address(es)
    private byte esmClass;
    private byte protocolId;
    private byte priorityFlag;
    private String scheduleDeliveryTime;
    private String validityPeriod;
    private byte registeredDelivery;
    private byte replaceIfPresentFlag;
    private byte dataCoding = EncodeString.LATIN1_int;
    private byte smDefaultMsgId;
    private short smLength;
    private String shortMessage = "";
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Others Fields">
    private TLVShort userMessageReference = new TLVShort(OPT_PAR_USER_MSG_REF);
    private TLVShort sourcePort = new TLVShort(OPT_PAR_SRC_PORT);
    private TLVByte sourceAddrSubunit = new TLVByte(OPT_PAR_SRC_ADDR_SUBUNIT);
    private TLVShort destinationPort = new TLVShort(OPT_PAR_DST_PORT);
    private TLVByte destAddrSubunit = new TLVByte(OPT_PAR_DST_ADDR_SUBUNIT);
    private TLVShort sarMsgRefNum = new TLVShort(OPT_PAR_SAR_MSG_REF_NUM);
    private TLVUByte sarTotalSegments = new TLVUByte(OPT_PAR_SAR_TOT_SEG);
    private TLVUByte sarSegmentSeqnum = new TLVUByte(OPT_PAR_SAR_SEG_SNUM);
    private TLVByte payloadType = new TLVByte(OPT_PAR_PAYLOAD_TYPE);
    private TLVOctets messagePayload = new TLVOctets(OPT_PAR_MSG_PAYLOAD, OPT_PAR_MSG_PAYLOAD_MIN, OPT_PAR_MSG_PAYLOAD_MAX);
    private TLVByte privacyIndicator = new TLVByte(OPT_PAR_PRIV_IND);
    private TLVOctets callbackNum = new TLVOctets(OPT_PAR_CALLBACK_NUM, OPT_PAR_CALLBACK_NUM_MIN, OPT_PAR_CALLBACK_NUM_MAX); // 4-19
    private TLVByte callbackNumPresInd = new TLVByte(OPT_PAR_CALLBACK_NUM_PRES_IND);
    private TLVOctets callbackNumAtag = new TLVOctets(OPT_PAR_CALLBACK_NUM_ATAG, OPT_PAR_CALLBACK_NUM_ATAG_MIN, OPT_PAR_CALLBACK_NUM_ATAG_MAX); // 1-65
    private TLVOctets sourceSubaddress = new TLVOctets(OPT_PAR_SRC_SUBADDR, OPT_PAR_SRC_SUBADDR_MIN, OPT_PAR_SRC_SUBADDR_MAX); // 2-23
    private TLVOctets destSubaddress = new TLVOctets(OPT_PAR_DEST_SUBADDR, OPT_PAR_DEST_SUBADDR_MIN, OPT_PAR_DEST_SUBADDR_MAX);
    private TLVByte displayTime = new TLVByte(OPT_PAR_DISPLAY_TIME);
    private TLVShort smsSignal = new TLVShort(OPT_PAR_SMS_SIGNAL);
    private TLVByte msValidity = new TLVByte(OPT_PAR_MS_VALIDITY);
    private TLVByte msMsgWaitFacilities = new TLVByte(OPT_PAR_MSG_WAIT); // bit mask
    private TLVEmpty alertOnMsgDelivery = new TLVEmpty(OPT_PAR_ALERT_ON_MSG_DELIVERY);
    private TLVByte languageIndicator = new TLVByte(OPT_PAR_LANG_IND);
    //</editor-fold>

    public SubmitMulti() {
        super(CommandId.SUBMIT_MULTI);
        ArrayList<TLV> reg = new ArrayList<TLV>(COUNT_OPTIMAl_PARAM);
        reg.add(userMessageReference);
        reg.add(sourcePort);
        reg.add(sourceAddrSubunit);
        reg.add(destinationPort);
        reg.add(destAddrSubunit);
        reg.add(sarMsgRefNum);
        reg.add(sarTotalSegments);
        reg.add(sarSegmentSeqnum);
        reg.add(payloadType);
        reg.add(messagePayload);
        reg.add(privacyIndicator);
        reg.add(callbackNum);
        reg.add(callbackNumPresInd);
        reg.add(callbackNumAtag);
        reg.add(sourceSubaddress);
        reg.add(destSubaddress);
        reg.add(displayTime);
        reg.add(smsSignal);
        reg.add(msValidity);
        reg.add(msMsgWaitFacilities);
        reg.add(alertOnMsgDelivery);
        reg.add(languageIndicator);
        registerOptionalParam(reg);
    }

    @Override
    public ChannelBuffer getBytesFromData() {

        ChannelBuffer buff = ChannelBuffers.buffer(MAX_LEN_BYTES);
        writeToChannelBufferFromCString(buff, serviceType);
        buff.writeBytes(sourceAddress.getBytesFromData());
        buff.writeByte(numberOfDests);
        buff.writeByte(esmClass);
        buff.writeByte(protocolId);
        buff.writeByte(priorityFlag);
        writeToChannelBufferFromCString(buff, scheduleDeliveryTime);
        writeToChannelBufferFromCString(buff, validityPeriod);
        buff.writeByte(registeredDelivery);
        buff.writeByte(replaceIfPresentFlag);
        buff.writeByte(dataCoding);
        buff.writeByte(smDefaultMsgId);
        buff.writeByte(encodeUnsigned(smLength));
        buff.writeBytes(shortMessage.getBytes(EncodeString.getCharSet(dataCoding)));

        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setServiceType(getCString(buffer));
        sourceAddress.setDataFromBytes(buffer);
        numberOfDests = buffer.readByte();
        //dest_address(es)
        esmClass = buffer.readByte();
        protocolId = buffer.readByte();
        priorityFlag = buffer.readByte();
        setScheduleDeliveryTime(getCString(buffer));
        setValidityPeriod(getCString(buffer));
        registeredDelivery = buffer.readByte();
        replaceIfPresentFlag = buffer.readByte();
        dataCoding = buffer.readByte();
        smDefaultMsgId = buffer.readByte();
        smLength = buffer.readUnsignedByte();
        ChannelBuffer readBytes = buffer.readBytes(smLength);
        shortMessage = new String(readBytes.array(), EncodeString.getCharSet(dataCoding));
    }

    @Override
    public Response createResponse() {
        return new SubmitSmResp();
    }

    //<editor-fold defaultstate="collapsed" desc="Main setters">
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public void setSourceAddress(byte ton, byte npi, String address) {
        sourceAddress.set(ton, npi, address);
    }

    public void setNumberOfDests(byte numberOfDests) {
        this.numberOfDests = numberOfDests;
    }

    public void setScheduleDeliveryTime(String scheduleDeliveryTime) {
        this.scheduleDeliveryTime = scheduleDeliveryTime;
    }

    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public void setEsmClass(byte value) {
        esmClass = value;
    }

    public void setProtocolId(byte value) {
        protocolId = value;
    }

    public void setPriorityFlag(byte value) {
        priorityFlag = value;
    }

    public void setRegisteredDelivery(byte value) {
        registeredDelivery = value;
    }

    public void setReplaceIfPresentFlag(byte value) {
        replaceIfPresentFlag = value;
    }

    public void setDataCoding(byte value) {
        dataCoding = value;
    }

    public void setSmDefaultMsgId(byte value) {
        smDefaultMsgId = value;
    }

    /*private void setSmLength(short smLength) {
     shortMessage.setLength = smLength;
     }*/
    public void setShortMessage(String message) {
        byte[] temp = message.getBytes(EncodeString.getCharSet(dataCoding));
        validationString(0, SmLenField.SM_MSG_LEN, temp.length);
        shortMessage = message;
        smLength = (short) temp.length;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Others setters">
    public void setUserMessageReference(short value) {
        userMessageReference.setValue(value);
    }

    public void setSourcePort(short value) {
        sourcePort.setValue(value);
    }

    public void setSourceAddrSubunit(byte value) {
        sourceAddrSubunit.setValue(value);
    }

    public void setDestinationPort(short value) {
        destinationPort.setValue(value);
    }

    public void setDestAddrSubunit(byte value) {
        destAddrSubunit.setValue(value);
    }

    public void setSarMsgRefNum(short value) {
        sarMsgRefNum.setValue(value);
    }

    public void setSarTotalSegments(short value) {
        sarTotalSegments.setValue(value);
    }

    public void setSarSegmentSeqnum(short value)
            throws IntegerOutOfRangeException {
        sarSegmentSeqnum.setValue(value);
    }

    public void setPayloadType(byte value) {
        payloadType.setValue(value);
    }

    public void setMessagePayload(byte[] value)
            throws InvalidLengthOptinalParametrException {
        messagePayload.setValue(value);
    }

    public void setPrivacyIndicator(byte value) {
        privacyIndicator.setValue(value);
    }

    public void setCallbackNum(byte[] value)
            throws InvalidLengthOptinalParametrException {
        callbackNum.setValue(value);
    }

    public void setCallbackNumPresInd(byte value) {
        callbackNumPresInd.setValue(value);
    }

    public void setCallbackNumAtag(byte[] value)
            throws InvalidLengthOptinalParametrException {
        callbackNumAtag.setValue(value);
    }

    public void setSourceSubaddress(byte[] value)
            throws InvalidLengthOptinalParametrException {
        sourceSubaddress.setValue(value);
    }

    public void setDestSubaddress(byte[] value)
            throws InvalidLengthOptinalParametrException {
        destSubaddress.setValue(value);
    }

    public void setDisplayTime(byte value) {
        displayTime.setValue(value);
    }

    public void setSmsSignal(short value) {
        smsSignal.setValue(value);
    }

    public void setMsValidity(byte value) {
        msValidity.setValue(value);
    }

    public void setMsMsgWaitFacilities(byte value) {
        msMsgWaitFacilities.setValue(value);
    }

    public void setAlertOnMsgDelivery(boolean value) {
        alertOnMsgDelivery.setValue(value);
    }

    public void setLanguageIndicator(byte value) {
        languageIndicator.setValue(value);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Main Getters">
    public String getServiceType() {
        return serviceType;
    }

    public Address getSourceAddress() {
        return sourceAddress;
    }

    public byte getNumberOfDests() {
        return numberOfDests;
    }

    public byte getEsmClass() {
        return esmClass;
    }

    public byte getProtocolId() {
        return protocolId;
    }

    public byte getPriorityFlag() {
        return priorityFlag;
    }

    public String getScheduleDeliveryTime() {
        return scheduleDeliveryTime;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public byte getRegisteredDelivery() {
        return registeredDelivery;
    }

    public byte getReplaceIfPresentFlag() {
        return replaceIfPresentFlag;
    }

    public byte getDataCoding() {
        return dataCoding;
    }

    public byte getSmDefaultMsgId() {
        return smDefaultMsgId;
    }

    public short getSmLength() {
        return smLength;
    }

    public String getShortMessage() {
        return shortMessage;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Others Getters">
    public boolean hasUserMessageReference() {
        return userMessageReference.hasValue();
    }

    public short getUserMessageReference() {
        return userMessageReference.getValue();
    }

    public short getSourcePort() {
        return sourcePort.getValue();
    }

    public byte getSourceAddrSubunit() {
        return sourceAddrSubunit.getValue();
    }

    public short getDestinationPort() {
        return destinationPort.getValue();
    }

    public byte getDestAddrSubunit() {
        return destAddrSubunit.getValue();
    }

    public short getSarMsgRefNum() {
        return sarMsgRefNum.getValue();
    }

    public short getSarTotalSegments() {
        return sarTotalSegments.getValue();
    }

    public short getSarSegmentSeqnum() {
        return sarSegmentSeqnum.getValue();
    }

    public byte getPayloadType() {
        return payloadType.getValue();
    }

    public ChannelBuffer getMessagePayload() {
        return messagePayload.getValue();
    }

    public byte getPrivacyIndicator() {
        return privacyIndicator.getValue();
    }

    public ChannelBuffer callbackNum() {
        return callbackNum.getValue();
    }

    public byte getCallbackNumPresInd() {
        return callbackNumPresInd.getValue();
    }

    public ChannelBuffer getCallbackNumAtag() {
        return callbackNumAtag.getValue();
    }

    public ChannelBuffer getSourceSubaddress() {
        return sourceSubaddress.getValue();
    }

    public ChannelBuffer getDestSubaddress() {
        return destSubaddress.getValue();
    }

    public byte getDisplayTime() {
        return displayTime.getValue();
    }

    public short getSmsSignal() {
        return smsSignal.getValue();
    }

    public byte getMsValidity() {
        return msValidity.getValue();
    }

    public byte getMsMsgWaitFacilities() {
        return msMsgWaitFacilities.getValue();
    }

    public boolean getAlertOnMsgDelivery() {
        return alertOnMsgDelivery.getValue();
    }

    public byte getLanguageIndicator() {
        return languageIndicator.getValue();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Has">
    public boolean hasSourcePort() {
        return sourcePort.hasValue();
    }

    public boolean hasSourceAddrSubunit() {
        return sourceAddrSubunit.hasValue();
    }

    public boolean hasDestinationPort() {
        return destinationPort.hasValue();
    }

    public boolean hasDestAddrSubunit() {
        return destAddrSubunit.hasValue();
    }

    public boolean hasSarMsgRefNum() {
        return sarMsgRefNum.hasValue();
    }

    public boolean hasSarTotalSegments() {
        return sarTotalSegments.hasValue();
    }

    public boolean hasSarSegmentSeqnum() {
        return sarSegmentSeqnum.hasValue();
    }

    public boolean hasPayloadType() {
        return payloadType.hasValue();
    }

    public boolean hasMessagePayload() {
        return messagePayload.hasValue();
    }

    public boolean hasPrivacyIndicator() {
        return privacyIndicator.hasValue();
    }

    public boolean hasCallbackNum() {
        return callbackNum.hasValue();
    }

    public boolean hasCallbackNumPresInd() {
        return callbackNumPresInd.hasValue();
    }

    public boolean hasCallbackNumAtag() {
        return callbackNumAtag.hasValue();
    }

    public boolean hasSourceSubaddress() {
        return sourceSubaddress.hasValue();
    }

    public boolean hasDestSubaddress() {
        return destSubaddress.hasValue();
    }

    public boolean hasDisplayTime() {
        return displayTime.hasValue();
    }

    public boolean hasSmsSignal() {
        return smsSignal.hasValue();
    }

    public boolean hasMsValidity() {
        return msValidity.hasValue();
    }

    public boolean hasMsMsgWaitFacilities() {
        return msMsgWaitFacilities.hasValue();
    }

    public boolean hasAlertOnMsgDelivery() {
        return alertOnMsgDelivery.hasValue();
    }

    public boolean hasLanguageIndicator() {
        return languageIndicator.hasValue();
    }

    //</editor-fold>
    @Override
    public String bodyToString() {
        StringBuilder debugString = new StringBuilder();
        debugString
                .append(" Body field-> ")
                .append(" serviceType= ").append(serviceType)
                .append(" sourse adres-> ").append(sourceAddress.toString())
                .append(" numberOfDests= ").append(numberOfDests)
                .append(" esmClass= ").append(esmClass)
                .append(" priorityFlag= ").append(priorityFlag)
                .append(" scheduleDeliveryTime= ").append(scheduleDeliveryTime)
                .append(" validityPeriod= ").append(validityPeriod)
                .append(" registeredDelivery= ").append(registeredDelivery)
                .append(" replaceIfPresentFlag= ").append(replaceIfPresentFlag)
                .append(" dataCoding= ").append(dataCoding)
                .append(" smDefaultMsgId= ").append(smDefaultMsgId)
                .append(" smLength= ").append(smLength)
                .append(" shortMessage= ").append(shortMessage);

        return debugString.toString();
    }

}
