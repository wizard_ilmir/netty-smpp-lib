/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;

/**
 *
 * @author SoftProfi
 */
public class SubmitSmResp
        extends Response {

    private String messageID;

    public SubmitSmResp() {
        super(CommandId.SUBMIT_SM_RESP);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        int maxLenBytes = lenCString(messageID);

        ChannelBuffer buff = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(buff, messageID);
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setMessageID(getCString(buffer));
    }

    public void setMessageID(String messageID) {
        validationCString(SmLenField.SM_MSGID_LEN, messageID);
        this.messageID = messageID;
    }

    public String getMessageID() {
        return messageID;
    }

    @Override
    public String bodyToString() {
        return " Body field-> " + " mesasgeID= " + messageID;
    }


}
