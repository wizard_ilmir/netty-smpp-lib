/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;

/**
 *
 * @author SoftProfi
 */
public class AlertNotification extends Pdu {

    private Address sourceAddress = new Address(SmLenField.SM_DATA_ADDR_LEN);
    private Address destAddress = new Address(SmLenField.SM_DATA_ADDR_LEN);

    public AlertNotification() {
        super(CommandId.ALERT_NOTIFICATION);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        ChannelBuffer sourceAddressBytes = sourceAddress.getBytesFromData();
        ChannelBuffer destAddressBytes = destAddress.getBytesFromData();
        int maxLenBytes = sourceAddressBytes.readableBytes() + destAddressBytes.readableBytes();

        ChannelBuffer buff = ChannelBuffers.buffer(maxLenBytes);
        buff.writeBytes(sourceAddressBytes);
        buff.writeBytes(destAddressBytes);
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        sourceAddress.setDataFromBytes(buffer);
        destAddress.setDataFromBytes(buffer);
    }

    @Override
    public boolean canResponse() {
        return false;
    }

    @Override
    public boolean isRequest() {
        return true;
    }

    @Override
    public boolean isResponse() {
        return false;
    }

    public void setSourceAddress(byte ton, byte npi, String address) {
        sourceAddress.set(ton, npi, address);
    }

    public void setDestAddress(byte ton, byte npi, String address) {
        destAddress.set(ton, npi, address);
    }

    public Address getSourceAddress() {
        return sourceAddress;
    }

    public Address getDestAddress() {
        return destAddress;
    }

    @Override
    public String bodyToString() {
        StringBuilder debugString = new StringBuilder();
        debugString
                .append(" sourceAddress= ").append(sourceAddress.toString())
                .append(" destAddress= ").append(destAddress);
        return debugString.toString();
    }

}
