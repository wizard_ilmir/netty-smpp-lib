/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;

/**
 *
 * @author SoftProfi
 */
public class QuerySmResp
        extends Response {

    private static final int FIXED_LEN_BYTES = 2;
    private String messageId;
    private String finalDate;
    private byte messageState;
    private byte errorCode;

    public QuerySmResp() {
        super(CommandId.QUERY_SM_RESP);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        int maxLenBytes = FIXED_LEN_BYTES + lenCString(messageId) + lenCString(finalDate);

        ChannelBuffer buff = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(buff, messageId);
        writeToChannelBufferFromCString(buff, finalDate);
        buff.writeByte(messageState);
        buff.writeByte(errorCode);
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setMessageId(getCString(buffer));
        setFinalDate(getCString(buffer));
        messageState = buffer.readByte();
        errorCode = buffer.readByte();
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setMessageState(byte messageState) {
        validationCString(SmLenField.SM_MSGID_LEN, messageId);
        this.messageState = messageState;
    }

    public void setErrorCode(byte errorCode) {
        this.errorCode = errorCode;
    }

    private void setFinalDate(String finalDate) {
        validationCString(SmLenField.SM_DATE_LEN, finalDate);
        this.finalDate = finalDate;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getFinalDate() {
        return finalDate;
    }

    public byte getMessageState() {
        return messageState;
    }

    public byte getErrorCode() {
        return errorCode;
    }

    @Override
    public String bodyToString() {
        StringBuilder debugString = new StringBuilder();
        debugString
                .append(" Body field-> ")
                .append(" messageId= ").append(messageId)
                .append(" finalDate= ").append(finalDate)
                .append(" messageState= ").append(messageState)
                .append(" errorCode= ").append(errorCode);

        return debugString.toString();
    }

}
