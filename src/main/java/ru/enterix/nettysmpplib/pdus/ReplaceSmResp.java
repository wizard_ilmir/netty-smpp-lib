/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.enterix.nettysmpplib.constants.CommandId;

/**
 *
 * @author SoftProfi
 */
public class ReplaceSmResp extends Response {

    public ReplaceSmResp() {
        super(CommandId.REPLACE_SM_RESP);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        return null;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) {

    }

}
