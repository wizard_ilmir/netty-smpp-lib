/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;

/**
 *
 * @author SoftProfi
 */
public class SubmitMultiResp
        extends Response {

    private static final int MAX_LEN_BYTES = SmLenField.SM_MSGID_LEN + 1;
    private String messageId;
    private byte no_unsuccess;
    //unsuccess_sme(s)  

    public SubmitMultiResp() {
        super(CommandId.SUBMIT_MULTI_RESP);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        ChannelBuffer buff = ChannelBuffers.buffer(MAX_LEN_BYTES);
        writeToChannelBufferFromCString(buff, messageId);
        buff.writeByte(no_unsuccess);
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setMessageId(getCString(buffer));
        no_unsuccess = buffer.readByte();

    }

    public void setMessageId(String messageId) {
        validationCString(SmLenField.SM_MSGID_LEN, messageId);
        this.messageId = messageId;
    }

    public void setNo_unsuccess(byte no_unsuccess) {
        this.no_unsuccess = no_unsuccess;
    }

    public String getMessageId() {
        return messageId;
    }

    public byte getNo_unsuccess() {
        return no_unsuccess;
    }

    @Override
    public String bodyToString() {
        StringBuilder str = new StringBuilder();
        str
                .append(" Body field-> ")
                .append(" messageId=").append(messageId)
                .append(" no_unsuccesso=").append(no_unsuccess);

        return super.toString() + str.toString();
    }

}
