/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

/**
 *
 * @author SoftProfi
 */
public abstract class Response
        extends Pdu {

    public Response(int id) {
        super(id);
    }

    @Override
    public final boolean canResponse() {
        return false;
    }

    @Override
    public final boolean isResponse() {
        return true;
    }

    @Override
    public final boolean isRequest() {
        return false;
    }

}
