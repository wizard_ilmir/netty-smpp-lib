/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;

/**
 *
 * @author SoftProfi
 */
public class CancelSm extends Request {

    private String serviceType;
    private String messageId;
    private Address sourceAddress = new Address();
    private Address destAddress = new Address();

    public CancelSm() {
        super(CommandId.CANCEL_SM);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        ChannelBuffer sourceAddressBytes = sourceAddress.getBytesFromData();
        ChannelBuffer destAddressBytes = destAddress.getBytesFromData();
        int maxLenBytes = (lenCString(serviceType) + lenCString(messageId) + sourceAddressBytes.readableBytes()
                + destAddressBytes.readableBytes());

        ChannelBuffer buff = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(buff, serviceType);
        writeToChannelBufferFromCString(buff, messageId);
        buff.writeBytes(sourceAddressBytes);
        buff.writeBytes(destAddressBytes);
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setServiceType(getCString(buffer));
        setMessageId(getCString(buffer));
        sourceAddress.setDataFromBytes(buffer);
        destAddress.setDataFromBytes(buffer);
    }

    @Override
    public Response createResponse() {
        return new CancelSmResp();
    }

    public void setServiceType(String serviceType) {
        validationCString(SmLenField.SM_SRVTYPE_LEN, serviceType);
        this.serviceType = serviceType;
    }

    public void setMessageId(String messageId) {
        validationCString(SmLenField.SM_MSGID_LEN, messageId);
        this.messageId = messageId;
    }

    public void setSourceAddress(byte ton, byte npi, String address) {
        sourceAddress.set(ton, npi, address);
    }

    public void setDestAddress(byte ton, byte npi, String address) {
        destAddress.set(ton, npi, address);
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getMessageId() {
        return messageId;
    }

    public Address getSourceAddress() {
        return sourceAddress;
    }

    public Address getDestAddress() {
        return destAddress;
    }

    @Override
    public String bodyToString() {
        StringBuilder debugString = new StringBuilder();
        debugString
                .append(" Body field-> ")
                .append(" serviceType= ").append(serviceType)
                .append(" messageId= ").append(messageId)
                .append(" sourceAddress= ").append(sourceAddress)
                .append(" destAddress= ").append(destAddress);
        return debugString.toString();
    }

}
