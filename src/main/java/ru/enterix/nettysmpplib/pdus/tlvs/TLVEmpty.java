/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus.tlvs;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.enterix.nettysmpplib.exceptions.ValueNotSetException;

/**
 *
 * @author SoftProfi
 */
public class TLVEmpty
extends TLV
{

	private boolean present = false;

	public TLVEmpty (short tag)
	{
		super(tag, 0, 0);
	}

	@Override
	protected void setValuefromBytes (ChannelBuffer buffer) 
	throws IndexOutOfBoundsException
	{
		present=true;
	}

	@Override
	protected ChannelBuffer  getValueFromBytes() 
	{
		if(hasValue()){
			return null;
		}else{
			throw new ValueNotSetException();
		}
		 
	}

	public void setValue (boolean present)
	{
		this.present = present;
		markValueSet();
	}

	public boolean getValue() throws ValueNotSetException
	{
		if (hasValue()) {
			return present;
		} else {
			throw new ValueNotSetException();
		}
	}

	@Override
	public boolean equals (Object obj)
	{
		if (obj == null || !(obj instanceof TLVEmpty)) {
			return false;
		}
		TLVEmpty tlvEmpty=(TLVEmpty)obj;
		return super.equals(obj) && tlvEmpty.present==present; //To change body of generated methods, choose Tools | Templates.
	}

    @Override
    public String valueToString ()
    {
        return Boolean.toString(present);
    }

        
	

}
