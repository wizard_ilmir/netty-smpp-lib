/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus.tlvs;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.exceptions.NotEnoughDataInByteBufferException;
import ru.enterix.nettysmpplib.exceptions.TerminatingZeroNotFoundException;
import ru.enterix.nettysmpplib.exceptions.ValueNotSetException;

/**
 *
 * @author SoftProfi
 */
public class TLVString
extends TLV
{

	private String value;

	public TLVString (short tag, int minLength, int maxLength)
	{
		super(tag, minLength, maxLength);
	}

	@Override
	protected void setValuefromBytes (ChannelBuffer buffer)
	throws IndexOutOfBoundsException, TerminatingZeroNotFoundException
	{
		try {
			value=getCString(buffer);
		} catch (NotEnoughDataInByteBufferException ex) {
			// сюда приходит нужное колличество байт, если байт не хватает то TLV сам выбросит  ошибку
		}
	}

	@Override
	protected ChannelBuffer getValueFromBytes ()
	{
		if (hasValue()) {
			ChannelBuffer buff = ChannelBuffers.buffer(value.length()+1);// + ноль в конце строки
			writeToChannelBufferFromCString(buff, value);
			return buff;
		} else {
			throw new ValueNotSetException();
		}
	}

	public void setValue (String value) 
	{
		validationCString(getMaxlength(), value);
		this.value = value;
		markValueSet();
	}

	public String getValue ()
	throws ValueNotSetException
	{
		if (hasValue()) {
			return value;
		} else {
			throw new ValueNotSetException();
		}

	}

	@Override
	public int getLen ()
	{
		return value.length()+1;
	}
	

	@Override
	public String toString ()
	{
		return super.toString() + " value=" + value;
	}


    @Override
    public String valueToString ()
    {
        return value;
    }

}
