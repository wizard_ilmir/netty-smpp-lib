
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus.tlvs;

//~--- non-JDK imports --------------------------------------------------------
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.exceptions.ValueNotSetException;

/**
 *
 * @author SoftProfi
 */
public class TLVByte
extends TLV
{

	public static final int MAX_LEN_BYTES = TLV_HEAD_LEN_BYTES + 1;
	private byte value;

	public TLVByte (short tag)
	{
		super(tag, 1, 1);
	}

	public TLVByte (short tag, byte value)
	{
		super(tag, 1, 1);
		this.value = value;
		markValueSet();
	}

	@Override
	protected void setValuefromBytes (ChannelBuffer buffer)
	throws IndexOutOfBoundsException
	{
		value = buffer.readByte();
	}

	@Override
	protected ChannelBuffer getValueFromBytes () 
	{
		if (hasValue()) {
			ChannelBuffer buff = ChannelBuffers.buffer(1);
			buff.writeByte(value);
			return buff;
		} else {
			throw new ValueNotSetException();
		}
	}

	public void setValue (byte value)
	{
		this.value = value;
		markValueSet();
	}

	public byte getValue() throws ValueNotSetException
	{
		if (hasValue()) {
			return value;
		} else {
			throw new ValueNotSetException();
		}
	}

	@Override
	public String toString ()
	{
		return super.toString() + " value=" + value;
	}

	@Override
	public boolean equals (Object obj)
	{
		if (obj == null || !(obj instanceof TLVByte)) {
			return false;
		}
		TLVByte tlvByte = (TLVByte) obj;
		return super.equals(obj) && tlvByte.value == value;
	}

    @Override
    public String valueToString ()
    {
        return Byte.toString(value);
    }

}
