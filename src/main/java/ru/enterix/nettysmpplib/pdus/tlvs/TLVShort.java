/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus.tlvs;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.exceptions.ValueNotSetException;

/**
 *
 * @author SoftProfi
 */
public class TLVShort
extends TLV
{

	private short value;

	public TLVShort (short tag)
	{
		super(tag, 2, 2);
	}

	public TLVShort (short tag, short value)
	{
		super(tag, 2, 2);
		this.value = value;
		markValueSet();
	}

	@Override
	protected void setValuefromBytes (ChannelBuffer buffer)
	throws IndexOutOfBoundsException
	{
		value = buffer.readShort();
	}

	@Override
	protected ChannelBuffer getValueFromBytes ()
	{
		if (hasValue()) {
			ChannelBuffer buff = ChannelBuffers.buffer(2);
			buff.writeShort(value);
			return buff;
		} else {
			throw new ValueNotSetException();
		}
	}

	public short getValue ()
	throws ValueNotSetException
	{
		if (hasValue()) {
			return value;
		} else {
			throw new ValueNotSetException();
		}
	}

	public void setValue (short value)
	{
		this.value = value;
		markValueSet();
	}

	@Override
	public String toString ()
	{
		return super.toString() + " value=" + value;
	}

	@Override
	public boolean equals (Object obj)
	{
		if (obj == null || !(obj instanceof TLVShort)) {
			return false;
		}
		TLVShort tlvShort = (TLVShort) obj;
		return super.equals(obj) && tlvShort.value == value;
	}

    @Override
    public String valueToString ()
    {
        return Short.toString(value);
    }

}
