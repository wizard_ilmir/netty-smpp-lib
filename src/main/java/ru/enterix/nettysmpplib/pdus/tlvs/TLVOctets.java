/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus.tlvs;

import java.util.Arrays;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.exceptions.InvalidLengthOptinalParametrException;
import ru.enterix.nettysmpplib.exceptions.ValueNotSetException;

/**
 *
 * @author SoftProfi
 */
public class TLVOctets
extends TLV
{

	private ChannelBuffer value;

	public TLVOctets (short tag, int minLength, int maxLength)
	{
		super(tag, minLength, maxLength);
		value = ChannelBuffers.buffer(0);
	}

	@Override
	protected void setValuefromBytes (ChannelBuffer buffer)
	throws IndexOutOfBoundsException
	{
		value.resetReaderIndex();
		value = ChannelBuffers.buffer(buffer.capacity());
		buffer.readBytes(value);
	}

	@Override
	protected ChannelBuffer getValueFromBytes () throws ValueNotSetException
	{
		if (hasValue()) {
			return value;
		} else {
			throw new ValueNotSetException();
		}

	}

	

	public void  setValue (byte [] value) 
	throws InvalidLengthOptinalParametrException
	{
		if (value.length > getMaxlength() || value.length < getMinlength()) {
			throw new InvalidLengthOptinalParametrException();
		}
		this.value = ChannelBuffers.wrappedBuffer(value);
		markValueSet();
	}

	public ChannelBuffer getValue()
	throws ValueNotSetException
	{
		if (hasValue()) {
			return value;
		} else {
			throw new ValueNotSetException();
		}
	}

	@Override
	public int getLen ()
	{
		return value.capacity();
	}
	
	

	@Override
	public String toString ()
	{
		return super.toString() + " value=" + value;
	}

	@Override
	public boolean equals (Object obj)
	{
		if (obj == null || !(obj instanceof TLVOctets)) {
			return false;
		}
		TLVOctets tlvOctets = (TLVOctets) obj;
		return super.equals(obj) && Arrays.equals(tlvOctets.value.array(), value.array());
	}

    @Override
    public String valueToString ()
    {
        return "";
    }
        
        

}
