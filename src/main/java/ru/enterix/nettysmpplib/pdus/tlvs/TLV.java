/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus.tlvs;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.exceptions.InvalidLengthOptinalParametrException;
import ru.enterix.nettysmpplib.exceptions.NotEnoughDataInByteBufferException;
import ru.enterix.nettysmpplib.exceptions.TerminatingZeroNotFoundException;
import ru.enterix.nettysmpplib.exceptions.ValueNotSetException;
import ru.enterix.nettysmpplib.pdus.ByteData;

/**
 *
 * @author SoftProfi
 */
public abstract class TLV
extends ByteData
{

	public static final int TLV_HEAD_LEN_BYTES = 4;
	private short tag;//
	private int minlength;
	private int maxlength;//
	private boolean valueIsSet;

	public TLV (short tag, int minLength, int maxLength)
	{
		this.tag = tag;
		this.minlength = minLength;
		this.maxlength = maxLength;
	}

	protected abstract void setValuefromBytes (ChannelBuffer buffer) 
	throws IndexOutOfBoundsException, TerminatingZeroNotFoundException;

	protected abstract ChannelBuffer getValueFromBytes () 
	throws ValueNotSetException;

	@Override
	public final void setDataFromBytes (ChannelBuffer buffer)
	throws NotEnoughDataInByteBufferException, InvalidLengthOptinalParametrException, TerminatingZeroNotFoundException
	{
		try {
			int tlvBytesLen = buffer.readUnsignedShort();
			if (maxlength < tlvBytesLen || minlength > tlvBytesLen) {
				///throw new InvalidOptionalParamLengthException(
				//String.format("TLV tag=%d expected minLen=%d, maxLen=%d and it len=%d", tag, minlength, maxlength, tlvBytesLen));
			}
			setValuefromBytes(buffer.readBytes(tlvBytesLen));
			markValueSet();
		} catch (IndexOutOfBoundsException ex) {
			//throw new NotEnoughDataInByteBufferException(ex);
		}
	}

	@Override
	public final ChannelBuffer getBytesFromData ()
	{
		ChannelBuffer value = getValueFromBytes();
		int valueLen = value == null ? 0 : value.writerIndex();
		int tlvBytesLen = TLV_HEAD_LEN_BYTES + valueLen;
		ChannelBuffer buffTLV = ChannelBuffers.buffer(tlvBytesLen);
		buffTLV.writeShort(tag);
		buffTLV.writeShort(encodeUnsigned(valueLen));
		if(valueLen!=0){
			buffTLV.writeBytes(value);
		}
		return buffTLV;
	}
	
	public int getLen(){
		return  maxlength;
	}

	public short getTag ()
	{
		return tag;
	}

	public int getMinlength ()
	{
		return minlength;
	}

	public int getMaxlength ()
	{
		return maxlength;
	}

	public boolean hasValue ()
	{
		return valueIsSet;
	}

	protected void markValueSet ()
	{
		valueIsSet = true;
	}

	@Override
	public final int hashCode ()
	{
		return tag;
	}

	@Override
	public String toString ()
	{
		StringBuilder debugString = new StringBuilder();
		debugString
		.append(" Tlv name->").append(getClass().getSimpleName())
		.append(" tag=").append(tag);

		return debugString.toString();
	}
        
        public abstract String valueToString();
	@Override
	public boolean equals (Object obj)
	{
		if (obj == null || !(obj instanceof TLV)) {
			return false;
		}
		TLV tlv = (TLV) obj;
		return (tlv.tag == tag
				&& tlv.valueIsSet == valueIsSet
				&& tlv.maxlength == maxlength
				&& tlv.minlength == minlength);
	}

}
