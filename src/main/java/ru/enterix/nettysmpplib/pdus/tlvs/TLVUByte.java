/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus.tlvs;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.exceptions.IntegerOutOfRangeException;
import ru.enterix.nettysmpplib.exceptions.ValueNotSetException;

/**
 *
 * @author SoftProfi
 */
public class TLVUByte
extends TLV
{

	private short value;

	public TLVUByte (short tag)
	{
		super(tag, 1, 1);
	}

	public TLVUByte (short tag, short value)
	{
		super(tag, 1, 1);
		this.value = value;
		markValueSet();
	}

	@Override
	protected void setValuefromBytes (ChannelBuffer buffer)
	throws IndexOutOfBoundsException
	{
		value = buffer.readUnsignedByte();
		markValueSet();
	}

	@Override
	protected ChannelBuffer getValueFromBytes ()
	{
		if (hasValue()) {
			ChannelBuffer buff = ChannelBuffers.buffer(1);
			buff.writeByte(encodeUnsigned(value));
			return buff;
		} else {
			throw new ValueNotSetException();
		}
	}

	public void setValue (short value)
	throws IntegerOutOfRangeException
	{
		validationRange(0, value, 255);
		this.value = value;
		markValueSet();
	}

	public short getValue() throws ValueNotSetException
	{
		if (hasValue()) {
			return value;
		} else {
			throw new ValueNotSetException();
		}
	}

	@Override
	public boolean equals (Object obj)
	{
		if (obj == null || !(obj instanceof TLVUByte)) {
			return false;
		}
		TLVUByte tlvString = (TLVUByte) obj;

		return super.equals(obj) && tlvString.value == value;
	}

    @Override
    public String valueToString ()
    {
        return Short.toString(value);
    }

}
