/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus.tlvs;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.exceptions.ValueNotSetException;

/**
 *
 * @author SoftProfi
 */
public class TLVInt
extends TLV
{

	public static final int MAX_LEN_BYTES = TLV_HEAD_LEN_BYTES + 4;
	private int value;

	public TLVInt (short tag, int value)
	{
		super(tag, 4, 4);
		this.value = value;
		markValueSet();
	}

	public TLVInt (short tag)
	{
		super(tag, 4, 4);
	}

	@Override
	protected void setValuefromBytes (ChannelBuffer buffer)
	throws IndexOutOfBoundsException
	{
		value = buffer.readInt();
	}

	@Override
	protected ChannelBuffer getValueFromBytes () 
	{
		if (hasValue()) {
			ChannelBuffer buff = ChannelBuffers.buffer(4);
			buff.writeInt(value);
			return buff;
		} else {
			throw new ValueNotSetException();
		}
	}

	public void setValue (int value)
	{
		this.value = value;
		markValueSet();
	}

	public int getValue()
	throws ValueNotSetException
	{
		if (hasValue()) {
			return value;
		} else {
			throw new ValueNotSetException();
		}
	}

	@Override
	public String toString ()
	{
		return super.toString() + " value=" + value;
	}

	@Override
	public boolean equals (Object obj)
	{
		if (obj == null || !(obj instanceof TLVInt)) {
			return false;
		}
		TLVInt tlvInt = (TLVInt) obj;
		return super.equals(obj) && tlvInt.value == value;
	}

    @Override
    public String valueToString ()
    {
        return Integer.toHexString(value);
    }

}
