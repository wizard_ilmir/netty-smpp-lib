/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import ru.enterix.nettysmpplib.constants.CommandId;

/**
 *
 * @author SoftProfi
 */
public class BindTransceiver extends BindRequest{

    public BindTransceiver() {
	super(CommandId.BIND_TRANSCEIVER);
    }

    
    @Override
    public boolean isTransmitter() {
        return true;
    }

    @Override
    public boolean isReceiver() {
        return true;
    }

    @Override
    public Response createResponse() {
        return  new BindTransceiverResp();
    }

    
    
    
    
    
}
