/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import ru.enterix.nettysmpplib.constants.CommandId;

/**
 *
 * @author SoftProfi
 */
public class BindReceiverResp extends BindResponse{

    public BindReceiverResp() {
	super(CommandId.BIND_RECEIVER_RESP);
    }
    
}
