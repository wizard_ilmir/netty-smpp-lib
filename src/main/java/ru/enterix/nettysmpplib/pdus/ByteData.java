/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import java.io.UnsupportedEncodingException;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.MutableDateTime;
import ru.enterix.nettysmpplib.constants.EncodeString;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.IntegerOutOfRangeException;
import ru.enterix.nettysmpplib.exceptions.InvalidDateParseException;
import ru.enterix.nettysmpplib.exceptions.InvalidLengthStringException;
import ru.enterix.nettysmpplib.exceptions.NotEnoughDataInByteBufferException;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import ru.enterix.nettysmpplib.exceptions.TerminatingZeroNotFoundException;

/**
 *
 * @author SoftProfi
 */
public abstract class ByteData {

    protected static String DEFAULT_DATE = "000000000000000R";

    protected static String getCString(ChannelBuffer buffer)
            throws NotEnoughDataInByteBufferException, TerminatingZeroNotFoundException {
        int beginReadIndex = buffer.readerIndex();
        int countBytes = 0;
        boolean isOk = false;

        while (buffer.readable()) {
            if (0x00 == buffer.readByte()) {
                isOk = true;
                break;
            }
            countBytes++;
        }
        if (countBytes == 0) {
            return null;
        }

        ChannelBuffer bytesCString = ChannelBuffers.buffer(countBytes);
        buffer.getBytes(beginReadIndex, bytesCString, countBytes);

        if (!isOk) {
            TerminatingZeroNotFoundException ex = new TerminatingZeroNotFoundException();
            ex.setPduBuffer(buffer);
            ex.setStringBuffer(bytesCString);
            throw ex;
        }

        String result = null;

        try {
            result = new String(bytesCString.array(), EncodeString.US_ASCII);
        } catch (UnsupportedEncodingException ex) {
            // ASCII support String
        }

        return result;

    }

    protected static void writeToChannelBufferFromCString(ChannelBuffer where, String whence) {
        try {
            if (whence != null) {
                where.writeBytes(whence.getBytes(EncodeString.US_ASCII));
            }
            where.writeByte(0x00);
        } catch (UnsupportedEncodingException ex) {
        }
    }

    protected static void writeToChannelBufferFromDateTime(ChannelBuffer where, DateTime whence) {
        if (whence == null) {
            writeToChannelBufferFromCString(where, "000000000000000R");
            return;
        }
        if (whence.getZone() != DateTimeZone.UTC) {
            whence = whence.withZone(DateTimeZone.UTC);
        }
        whence = whence.plusMinutes(15);
        writeToChannelBufferFromCString(where, convertDateTimeUTC(whence));
    }

    protected static void validationCString(int max, String str) {
        validationCString(1, max, str);
    }

    protected static void validationCString(int min, int max, String str) {
        int length = str == null ? 1 : str.length() + 1;
        if (length > max || length < min) {
            String descriptionException = String.format("Expected size from %d to %d, and it %d", min, max, str.length());
            InvalidLengthStringException ex = new InvalidLengthStringException(descriptionException);
            ex.setMin(min);
            ex.setMax(max);
            ex.setSourceLength(length);
            ex.setSrt(str);
            throw ex;
        }
    }

    protected static void validationString(int min, int max, int len) {
        if (min > len || max < len) {
            String descriptionException = String.format("Expected size from %d to %d, and it %d", min, max, len);
            InvalidLengthStringException ex = new InvalidLengthStringException(descriptionException);
            ex.setMin(min);
            ex.setMax(max);
            ex.setSourceLength(len);
            throw ex;
        }
    }

    protected static String getDate(ChannelBuffer buffer)
            throws NotEnoughDataInByteBufferException, TerminatingZeroNotFoundException, InvalidDateParseException {
        String dateStr = getCString(buffer);
        if (dateStr == null) {
            return null;
        }
        if (dateStr.length() != SmLenField.SM_DATE_LEN - 1) {
///			throw new InvalidDateParseException(String.format("Invalid length parse string. Parse=%s, length=%d", dateStr,	 dateStr.length()));
        }
        return null;
    }

    protected static DateTime getDateTimeUTC(ChannelBuffer buffer)
            throws NotEnoughDataInByteBufferException, InvalidDateParseException, TerminatingZeroNotFoundException {
        String dateStr = getCString(buffer);
        if (dateStr == null) {
            //throw new InvalidDateParseException(String.format("Invalid length parse string. String==null"));
        }
        if (dateStr.length() != SmLenField.SM_DATE_LEN - 1) {
            //throw new InvalidDateParseException(String.format("Invalid length parse string. Parse=%s, length=%d", dateStr,
//															  dateStr.length()));
        }
        return getDateTimeUTC(dateStr);
    }

    protected static DateTime getDateTimeUTC(String dateStr)
            throws InvalidDateParseException {
        if (dateStr.length() != SmLenField.SM_DATE_LEN - 1) {
//			throw new InvalidDateParseException(String.format("Invalid length parse string. Parse=%s, length=%d", dateStr,
//															  dateStr.length()));
        }
        int monthMM = 0, dayDD = 0, hourhh = 0, minutemm = 0, secss = 0, millisecnn = 0, offsetRelativeUTC = 0;
        String yearString;
        String typeDate;

        yearString = dateStr.substring(0, 2);
        monthMM = Integer.valueOf(dateStr.substring(2, 4));
        dayDD = Integer.valueOf(dateStr.substring(4, 6));
        hourhh = Integer.valueOf(dateStr.substring(6, 8));
        minutemm = Integer.valueOf(dateStr.substring(8, 10));
        secss = Integer.valueOf(dateStr.substring(10, 12));
        millisecnn = Integer.valueOf(dateStr.substring(12, 13)) * 100;
        offsetRelativeUTC = Integer.valueOf(dateStr.substring(13, 15));
        typeDate = dateStr.substring(15, 16);

        if (typeDate.equals("R")) {
            int yearYY = Integer.valueOf(yearString);
            try {
                isPositive(yearYY);
                isPositive(monthMM);
                isPositive(dayDD);
                isPositive(hourhh);
                isPositive(minutemm);
                isPositive(secss);
                isPositive(millisecnn);
                isPositive(offsetRelativeUTC);
            } catch (Exception ex) {
                //throw new InvalidDateParseException("Invalid parse string to date. Parse=" + dateStr, ex);
            }
            if (!(millisecnn == 0 && offsetRelativeUTC == 0)) {
                ///throw new InvalidDateParseException(
                //"Invalid parse string to date. Offset and milliseconds should be installed 0.Parse="
                //+ dateStr);
            }
            MutableDateTime dateNew = new MutableDateTime();
            dateNew.addYears(yearYY);
            dateNew.addMonths(monthMM);
            dateNew.addDays(dayDD);
            dateNew.addHours(hourhh);
            dateNew.addMinutes(minutemm);
            dateNew.addSeconds(secss);
            return dateNew.toDateTime(DateTimeZone.UTC);
        } else {
            int yearYY = Integer.valueOf("20" + yearString);
            MutableDateTime dateNew = new MutableDateTime();
            try {
                dateNew = new MutableDateTime(yearYY, monthMM, dayDD, hourhh, minutemm, secss, millisecnn, DateTimeZone.forOffsetHours(
                        offsetRelativeUTC));
            } catch (IllegalFieldValueException ex) {
                //throw new InvalidDateParseException("Invalid parse string to date. Parse=" + dateStr, ex);
            }

            if (typeDate.equals("p") || typeDate.equals("+")) {
                dateNew.addMinutes(-15);
            } else if (typeDate.equals("-")) {
                dateNew.addMinutes(+15);
            } else {
                //throw new InvalidDateParseException("Invalid typeDate. Type=" + typeDate);
            }
            return dateNew.toDateTime(DateTimeZone.UTC);
        }
    }

    private static void isPositive(Integer value)
            throws Exception {
        if (value < 0) {
            throw new Exception("Field not positive");
        }

    }

    protected static String convertDateTimeUTC(DateTime dateUTC) {
        if (dateUTC.getZone() != DateTimeZone.UTC) {
            throw new RuntimeException("DateUTC.getZone!=UTC");
        }

        StringBuilder dateStr = new StringBuilder();
        dateStr.append(dateUTC.toString("yyMMddHHmmssS")).append("00+");

        return dateStr.toString();
    }

    protected abstract ChannelBuffer getBytesFromData();

    protected abstract void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException;

    protected static byte encodeUnsigned(short positive) {
        if (positive < 128) {
            return (byte) positive;
        } else {
            return (byte) (-(256 - positive));
        }
    }

    protected static short encodeUnsigned(int positive) {
        if (positive < 32768) {
            return (byte) positive;
        } else {
            return (short) (-(65536 - positive));
        }
    }

    protected static short decodeUnsigned(byte signed) {
        if (signed >= 0) {
            return signed;
        } else {
            return (short) (256 + (short) signed);
        }
    }

    protected static int decodeUnsigned(short signed) {
        if (signed >= 0) {
            return signed;
        } else {
            return (int) (65536 + (int) signed);
        }
    }

    protected static void validationRange(int min, int val, int max)
            throws IntegerOutOfRangeException {
        if ((val < min) || (val > max)) {
            throw new IntegerOutOfRangeException();
        }
    }

    protected static int lenCString(String value) {
        return value == null ? 1 : value.length() + 1;
    }

}
