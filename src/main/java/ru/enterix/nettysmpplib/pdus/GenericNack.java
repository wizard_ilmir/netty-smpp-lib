/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.enterix.nettysmpplib.constants.CommandId;

/**
 *
 * @author SoftProfi
 */
public class GenericNack
        extends Pdu {

    public GenericNack() {
        super(CommandId.GENERIC_NACK);
    }

    @Override
    protected ChannelBuffer getBytesFromData() {
        return null;
    }

    @Override
    protected void setDataFromBytes(ChannelBuffer buffer) {
    }

    @Override
    public boolean canResponse() {
        return false;
    }

    @Override
    public boolean isRequest() {
        return true;
    }

    @Override
    public boolean isResponse() {
        return false;
    }

}
