/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import java.util.ArrayList;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.EncodeString;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DST_PORT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_ITS_SESSION_INFO;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_LANG_IND;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_STATE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_NW_ERR_CODE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_NW_ERR_CODE_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_NW_ERR_CODE_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_PAYLOAD_TYPE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_PRIV_IND;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_RECP_MSG_ID;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_RECP_MSG_ID_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_RECP_MSG_ID_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_MSG_REF_NUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_SEG_SNUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_TOT_SEG;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_PORT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_USER_MSG_REF;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_USER_RESP_CODE;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.InvalidLengthOptinalParametrException;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import ru.enterix.nettysmpplib.pdus.tlvs.TLV;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVByte;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVOctets;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVShort;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVString;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVUByte;

/**
 *
 * @author SoftProfi
 */
public class DeliverSm extends Request {

    //<editor-fold defaultstate="collapsed" desc="Main Fields">
    private static final int FIXED_LEN_BYTES = 8;
    private static final int COUNT_OPTIMAL_PARAM = 18;
    private String serviceType;
    private Address sourceAddress = new Address();
    private Address destAddress = new Address();
    private byte esmClass;
    private byte protocolId;
    private byte priorityFlag;
    private String scheduleDeliveryTime;
    private String validityPeriod;
    private byte registeredDelivery;
    private byte replaceIfPresentFlag;
    private byte dataCoding = EncodeString.LATIN1_int;
    private byte smDefaultMsgId;
    private short smLength = 0;
    private String shortMessage = "";
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Other Fields">
    private TLVShort userMessageReference = new TLVShort(OPT_PAR_USER_MSG_REF);
    private TLVShort sourcePort = new TLVShort(OPT_PAR_SRC_PORT);
    private TLVShort destinationPort = new TLVShort(OPT_PAR_DST_PORT);
    private TLVShort sarMsgRefNum = new TLVShort(OPT_PAR_SAR_MSG_REF_NUM);
    private TLVUByte sarTotalSegments = new TLVUByte(OPT_PAR_SAR_TOT_SEG);
    private TLVUByte sarSegmentSeqnum = new TLVUByte(OPT_PAR_SAR_SEG_SNUM);
    private TLVByte payloadType = new TLVByte(OPT_PAR_PAYLOAD_TYPE);
    private TLVOctets messagePayload = new TLVOctets(OPT_PAR_MSG_PAYLOAD, OPT_PAR_MSG_PAYLOAD_MIN, OPT_PAR_MSG_PAYLOAD_MAX);
    private TLVByte privacyIndicator = new TLVByte(OPT_PAR_PRIV_IND);
    private TLVOctets callbackNum = new TLVOctets(OPT_PAR_CALLBACK_NUM, OPT_PAR_CALLBACK_NUM_MIN, OPT_PAR_CALLBACK_NUM_MAX); // 4-19
    private TLVOctets sourceSubaddress = new TLVOctets(OPT_PAR_SRC_SUBADDR, OPT_PAR_SRC_SUBADDR_MIN, OPT_PAR_SRC_SUBADDR_MAX); // 2-23 
    private TLVOctets destSubaddress = new TLVOctets(OPT_PAR_DEST_SUBADDR, OPT_PAR_DEST_SUBADDR_MIN, OPT_PAR_DEST_SUBADDR_MAX);
    private TLVByte userResponseCode = new TLVByte(OPT_PAR_USER_RESP_CODE);
    private TLVByte languageIndicator = new TLVByte(OPT_PAR_LANG_IND);
    private TLVShort itsSessionInfo = new TLVShort(OPT_PAR_ITS_SESSION_INFO);
    private TLVOctets networkErrorCode = new TLVOctets(OPT_PAR_NW_ERR_CODE, OPT_PAR_NW_ERR_CODE_MIN, OPT_PAR_NW_ERR_CODE_MAX); // exactly 3
    private TLVByte messageState = new TLVByte(OPT_PAR_MSG_STATE);
    private TLVString receiptedMessageId = new TLVString(OPT_PAR_RECP_MSG_ID, OPT_PAR_RECP_MSG_ID_MIN, OPT_PAR_RECP_MSG_ID_MAX); // 1-
    //</editor-fold>

    public DeliverSm() {
        super(CommandId.DELIVER_SM);
        ArrayList<TLV> reg = new ArrayList<TLV>(COUNT_OPTIMAL_PARAM);
        reg.add(userMessageReference);
        reg.add(sourcePort);
        reg.add(destinationPort);
        reg.add(sarMsgRefNum);
        reg.add(sarTotalSegments);
        reg.add(sarSegmentSeqnum);
        reg.add(payloadType);
        reg.add(messagePayload);
        reg.add(privacyIndicator);
        reg.add(callbackNum);
        reg.add(sourceSubaddress);
        reg.add(destSubaddress);
        reg.add(userResponseCode);
        reg.add(languageIndicator);
        reg.add(itsSessionInfo);
        reg.add(networkErrorCode);
        reg.add(messageState);
        reg.add(receiptedMessageId);
        registerOptionalParam(reg);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        ChannelBuffer addressSourceBytes = sourceAddress.getBytesFromData();
        ChannelBuffer addressDestBytes = destAddress.getBytesFromData();
        int maxLenBytes = (FIXED_LEN_BYTES + lenCString(serviceType) + addressSourceBytes.readableBytes()
                + addressDestBytes.readableBytes() + lenCString(scheduleDeliveryTime)
                + lenCString(validityPeriod) + smLength);

        ChannelBuffer buff = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(buff, serviceType);
        buff.writeBytes(sourceAddress.getBytesFromData());
        buff.writeBytes(destAddress.getBytesFromData());
        buff.writeByte(esmClass);
        buff.writeByte(protocolId);
        buff.writeByte(priorityFlag);
        writeToChannelBufferFromCString(buff, scheduleDeliveryTime);
        writeToChannelBufferFromCString(buff, validityPeriod);
        buff.writeByte(registeredDelivery);
        buff.writeByte(replaceIfPresentFlag);
        buff.writeByte(dataCoding);
        buff.writeByte(smDefaultMsgId);
        buff.writeByte(encodeUnsigned(smLength));
        buff.writeBytes(shortMessage.getBytes(EncodeString.getCharSet(dataCoding)));
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setServiceType(getCString(buffer));
        sourceAddress.setDataFromBytes(buffer);
        destAddress.setDataFromBytes(buffer);
        esmClass = buffer.readByte();
        protocolId = buffer.readByte();
        priorityFlag = buffer.readByte();
        scheduleDeliveryTime = getCString(buffer);
        validityPeriod = getCString(buffer);
        registeredDelivery = buffer.readByte();
        replaceIfPresentFlag = buffer.readByte();
        dataCoding = buffer.readByte();
        smDefaultMsgId = buffer.readByte();
        smLength = buffer.readUnsignedByte();
        ChannelBuffer readBytes = buffer.readBytes(smLength);
        shortMessage = new String(readBytes.array(), EncodeString.getCharSet(dataCoding));
    }

    @Override
    public Response createResponse() {
        return new DeliverSmResp();
    }

    //<editor-fold defaultstate="collapsed" desc="Main setters">
    public void setServiceType(String value) {
        validationCString(SmLenField.SM_SRVTYPE_LEN, value);
        serviceType = value;
    }

    public void setSourceAddress(byte ton, byte npi, String address) {
        sourceAddress.set(ton, npi, address);
    }

    public void setDestAddress(byte ton, byte npi, String address) {
        destAddress.set(ton, npi, address);
    }

    public void setEsmClass(byte value) {
        esmClass = value;
    }

    public void setProtocolId(byte value) {
        protocolId = value;
    }

    public void setPriorityFlag(byte value) {
        priorityFlag = value;
    }

    public void setScheduleDeliveryTime(String scheduleDeliveryTime) {
        this.scheduleDeliveryTime = scheduleDeliveryTime;
    }

    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public void setRegisteredDelivery(byte value) {
        registeredDelivery = value;
    }

    public void setReplaceIfPresentFlag(byte replaceIfPresentFlag) {
        this.replaceIfPresentFlag = replaceIfPresentFlag;
    }

    public void setDataCoding(byte encode) {
        dataCoding = encode;
    }

    public void setSmDefaultMsgId(byte smDefaultMsgId) {
        this.smDefaultMsgId = smDefaultMsgId;
    }

    /*private void setSmLength(short smLength) {
     shortMessage.setLength = smLength;
     }*/
    public void setShortMessage(String message) {
        byte[] temp = message.getBytes(EncodeString.getCharSet(dataCoding));
        validationString(0, SmLenField.SM_MSG_LEN, temp.length);
        shortMessage = message;
        smLength = (short) temp.length;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Other setters">
    public void setUserMessageReference(short value) {
        userMessageReference.setValue(value);
    }

    public void setSourcePort(short value) {
        sourcePort.setValue(value);
    }

    public void setDestinationPort(short value) {
        destinationPort.setValue(value);
    }

    public void setSarMsgRefNum(short value) {
        sarMsgRefNum.setValue(value);
    }

    public void setSarTotalSegments(short value)
            {
        sarTotalSegments.setValue(value);
    }

    public void setSarSegmentSeqnum(short value)
            {
        sarSegmentSeqnum.setValue(value);
    }

    public void setPayloadType(byte value) {
        payloadType.setValue(value);
    }

    public void setMessagePayload(byte[] value)
            throws InvalidLengthOptinalParametrException {
        messagePayload.setValue(value);
    }

    public void setPrivacyIndicator(byte value) {
        privacyIndicator.setValue(value);
    }

    public void setCallbackNum(byte[] value)
            throws InvalidLengthOptinalParametrException {
        callbackNum.setValue(value);
    }

    public void setSourceSubaddress(byte[] value)
            throws InvalidLengthOptinalParametrException {
        sourceSubaddress.setValue(value);
    }

    public void setDestSubaddress(byte[] value)
            throws InvalidLengthOptinalParametrException {
        destSubaddress.setValue(value);
    }

    public void setUserResponseCode(byte value) {
        userResponseCode.setValue(value);
    }

    public void setLanguageIndicator(byte value) {
        languageIndicator.setValue(value);
    }

    public void setItsSessionInfo(short value) {
        itsSessionInfo.setValue(value);
    }

    public void setNetworkErrorCode(byte[] value)
            throws InvalidLengthOptinalParametrException {
        networkErrorCode.setValue(value);
    }

    public void setMessageState(byte value) {
        messageState.setValue(value);
    }

    public void setReceiptedMessageId(String value) {
        receiptedMessageId.setValue(value);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Main getter">
    public String getServiceType() {
        return serviceType;
    }

    public Address getSourceAddr() {
        return sourceAddress;
    }

    public Address getDestAddr() {
        return destAddress;
    }

    public byte getEsmClass() {
        return esmClass;
    }

    public byte getProtocolId() {
        return protocolId;
    }

    public byte getPriorityFlag() {
        return priorityFlag;
    }

    public String getScheduleDeliveryTime() {
        return scheduleDeliveryTime;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public byte getRegisteredDelivery() {
        return registeredDelivery;
    }

    public byte getReplaceIfPresentFlag() {
        return replaceIfPresentFlag;
    }

    public byte getDataCoding() {
        return dataCoding;
    }

    public byte getSmDefaultMsgId() {
        return smDefaultMsgId;
    }

    public short getSmLength() {
        return smLength;
    }

    public String getShortMessage() {
        return shortMessage;
    }

    //<editor-fold defaultstate="collapsed" desc="Others getter">
    //</editor-fold>
    public short getUserMessageReference() {
        return userMessageReference.getValue();
    }

    public short getSourcePort() {
        return sourcePort.getValue();
    }

    public short getDestinationPort() {
        return destinationPort.getValue();
    }

    public short getSarMsgRefNum() {
        return sarMsgRefNum.getValue();
    }

    public short getSarTotalSegments() {
        return sarTotalSegments.getValue();
    }

    public short getSarSegmentSeqnum() {
        return sarSegmentSeqnum.getValue();
    }

    public byte getPayloadType() {
        return payloadType.getValue();
    }

    public ChannelBuffer getMessagePayload() {
        return messagePayload.getValue();
    }

    public byte getPrivacyIndicator() {
        return privacyIndicator.getValue();
    }

    public ChannelBuffer callbackNum() {
        return callbackNum.getValue();
    }

    public ChannelBuffer getSourceSubaddress() {
        return sourceSubaddress.getValue();
    }

    public ChannelBuffer getDestSubaddress() {
        return destSubaddress.getValue();
    }

    public byte getUserResponseCode() {
        return userResponseCode.getValue();
    }

    public byte getLanguageIndicator() {
        return languageIndicator.getValue();
    }

    public short getItsSessionInfo() {
        return itsSessionInfo.getValue();
    }

    public ChannelBuffer getNetworkErrorCode() {
        return networkErrorCode.getValue();
    }

    public byte getMessageState() {
        return messageState.getValue();
    }

    public String getReceiptedMessageId() {
        return receiptedMessageId.getValue();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Has">
    public boolean hasUserMessageReference() {
        return userMessageReference.hasValue();
    }

    public boolean hasSourcePort() {
        return sourcePort.hasValue();
    }

    public boolean hasDestinationPort() {
        return destinationPort.hasValue();
    }

    public boolean hasSarMsgRefNum() {
        return sarMsgRefNum.hasValue();
    }

    public boolean hasSarTotalSegments() {
        return sarTotalSegments.hasValue();
    }

    public boolean hasSarSegmentSeqnum() {
        return sarSegmentSeqnum.hasValue();
    }

    public boolean hasPayloadType() {
        return payloadType.hasValue();
    }

    public boolean hasMessagePayload() {
        return messagePayload.hasValue();
    }

    public boolean hasPrivacyIndicator() {
        return privacyIndicator.hasValue();
    }

    public boolean hasCallbackNum() {
        return callbackNum.hasValue();
    }

    public boolean hasSourceSubaddress() {
        return sourceSubaddress.hasValue();
    }

    public boolean hasDestSubaddress() {
        return destSubaddress.hasValue();
    }

    public boolean hasUserResponseCode() {
        return userResponseCode.hasValue();
    }

    public boolean hasLanguageIndicator() {
        return languageIndicator.hasValue();
    }

    public boolean hasItsSessionInfo() {
        return itsSessionInfo.hasValue();
    }

    public boolean hasNetworkErrorCode() {
        return networkErrorCode.hasValue();
    }

    public boolean hasMessageState() {
        return messageState.hasValue();
    }

    public boolean hasReceiptedMessageId() {
        return receiptedMessageId.hasValue();
    }
    //</editor-fold>

    @Override
    public String bodyToString() {
        StringBuilder body = new StringBuilder();
        body
                .append(" Body field-> ")
                .append(" serviceType= ").append(serviceType)
                .append(" sourceAddress-> ").append(sourceAddress)
                .append(" destAddress-> ").append(destAddress)
                .append(" esmClass= ").append(esmClass)
                .append(" protocolId= ").append(protocolId)
                .append(" priorityFlag= ").append(priorityFlag)
                .append(" scheduleDeliveryTime= ").append(scheduleDeliveryTime)
                .append(" validityPeriod= ").append(validityPeriod)
                .append(" registeredDelivery= ").append(registeredDelivery)
                .append(" replaceIfPresentFlag= ").append(replaceIfPresentFlag)
                .append(" dataCoding= ").append(dataCoding)
                .append(" smDefaultMsgId= ").append(smDefaultMsgId)
                .append(" smLength= ").append(smLength)
                .append(" shortMessage= ").append(shortMessage);
        return body.toString();
    }

}
