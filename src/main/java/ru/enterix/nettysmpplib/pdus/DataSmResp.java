/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import java.util.ArrayList;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_ADD_STAT_INFO;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_ADD_STAT_INFO_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_ADD_STAT_INFO_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEL_FAIL_RSN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DPF_RES;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_NW_ERR_CODE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_NW_ERR_CODE_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_NW_ERR_CODE_MIN;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import static ru.enterix.nettysmpplib.pdus.ByteData.lenCString;
import ru.enterix.nettysmpplib.pdus.tlvs.TLV;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVByte;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVOctets;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVString;

/**
 *
 * @author SoftProfi
 */
class DataSmResp extends Response {

    private static final int COUNT_OPTIMAL_PARAM = 4;
    private String messageId;

    //<editor-fold defaultstate="collapsed" desc="Other Fields">
    private TLVByte deliveryFailureReason = new TLVByte(OPT_PAR_DEL_FAIL_RSN);
    private TLVOctets networkErrorCode = new TLVOctets(OPT_PAR_NW_ERR_CODE, OPT_PAR_NW_ERR_CODE_MIN, OPT_PAR_NW_ERR_CODE_MAX);
    private TLVString additionalStatusInfoText = new TLVString(OPT_PAR_ADD_STAT_INFO, OPT_PAR_ADD_STAT_INFO_MIN, OPT_PAR_ADD_STAT_INFO_MAX);
    private TLVByte dpfResult = new TLVByte(OPT_PAR_DPF_RES);
    //</editor-fold>

    public DataSmResp() {
        super(CommandId.DATA_SM_RESP);
        ArrayList<TLV> reg = new ArrayList<TLV>(COUNT_OPTIMAL_PARAM);
        reg.add(deliveryFailureReason);
        reg.add(networkErrorCode);
        reg.add(additionalStatusInfoText);
        reg.add(dpfResult);
        registerOptionalParam(reg);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        int maxLenBytes = lenCString(messageId);
        ChannelBuffer buff = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(buff, messageId);
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setMessageId(getCString(buffer));
    }

    public void setMessageId(String messageId) {
        validationCString(SmLenField.SM_MSG_LEN, messageId);
        this.messageId = messageId;
    }
    //<editor-fold defaultstate="collapsed" desc="Getters Fields">

    public String getMessageId() {
        return messageId;
    }

    public byte getDeliveryFailureReason() {
        return deliveryFailureReason.getValue();
    }

    public ChannelBuffer getNetworkErrorCode() {
        return networkErrorCode.getValue();
    }

    public String getAdditionalStatusInfoText() {
        return additionalStatusInfoText.getValue();
    }

    public byte getDpfResult() {
        return dpfResult.getValue();
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Setters">
    public void setDeliveryFailureReason(byte value) {
        this.deliveryFailureReason.setValue(value);
    }

    public void setNetworkErrorCode(byte[] value) {
        this.networkErrorCode.setValue(value);
    }

    public void setAdditionalStatusInfoText(String value) {
        this.additionalStatusInfoText.setValue(value);
    }

    public void setDpfResult(byte value) {
        this.dpfResult.setValue(value);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Has">
    public boolean hasDeliveryFailureReason() {
        return this.deliveryFailureReason.hasValue();
    }

    public boolean hasNetworkErrorCode() {
        return this.networkErrorCode.hasValue();
    }

    public boolean hasAdditionalStatusInfoText() {
        return this.additionalStatusInfoText.hasValue();
    }

    public boolean hasDpfResult() {
        return this.dpfResult.hasValue();
    }
//</editor-fold>
}
