/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.enterix.nettysmpplib.constants.CommandId;

/**
 *
 * @author SoftProfi
 */
/*Отправляется  при недопустимом заголовке  */
public class UnbindResp extends Response {

    public UnbindResp() {
        super(CommandId.UNBIND_RESP);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        return null;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) {
    }

}
