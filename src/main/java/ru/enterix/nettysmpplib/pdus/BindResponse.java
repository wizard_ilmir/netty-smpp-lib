/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import java.util.ArrayList;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.OptionalTag;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import ru.enterix.nettysmpplib.pdus.tlvs.TLV;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVByte;

/**
 *
 * @author SoftProfi
 */
public class BindResponse extends Response {

    private static final int COUNT_OPTIMAL_PARAM = 1;
    private String systemId;
    private TLVByte scInterfaceVersion = new TLVByte(OptionalTag.OPT_PAR_SC_IF_VER, (byte) 0x34);

    public BindResponse(int id) {
        super(id);
        ArrayList<TLV> reg = new ArrayList<TLV>(COUNT_OPTIMAL_PARAM);
        reg.add(scInterfaceVersion);
        registerOptionalParam(reg);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        int maxLenBytes = lenCString(systemId);
        ChannelBuffer buffer = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(buffer, systemId);
        return buffer;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        if (buffer.readable()) {
            setSystemId(getCString(buffer));
        }
    }

    public void setSystemId(String systemId) {
        validationCString(SmLenField.SM_SYSID_LEN, systemId);
        this.systemId = systemId;
    }

    public String getSystemId() {
        return systemId;
    }

    @Override
    public String bodyToString() {
        StringBuilder debugString = new StringBuilder();
        debugString
                .append(" Body field-> ")
                .append(" systemId= ").append(systemId);

        return debugString.toString();
    }

}
