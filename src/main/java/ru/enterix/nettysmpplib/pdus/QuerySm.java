/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;

/**
 *
 * @author SoftProfi
 */
public class QuerySm extends Request {

    private String messageId;
    private Address sourceAddress = new Address();

    public QuerySm() {
        super(CommandId.QUERY_SM);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        ChannelBuffer sourceAddressBytes = sourceAddress.getBytesFromData();
        int maxLenBytes = lenCString(messageId) + sourceAddressBytes.readableBytes();

        ChannelBuffer buff = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(buff, messageId);
        buff.writeBytes(sourceAddressBytes);
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setMessageId(getCString(buffer));
        sourceAddress.setDataFromBytes(buffer);
    }

    @Override
    public Response createResponse() {
        return new QuerySmResp();
    }

    public void setMessageId(String messageId) {
        validationCString(SmLenField.SM_MSGID_LEN, messageId);
        this.messageId = messageId;
    }

    public void setSourceAddress(byte ton, byte npi, String address) {
        sourceAddress.set(ton, npi, address);
    }

    public String getMessageId() {
        return messageId;
    }

    public Address getSourceAddress() {
        return sourceAddress;
    }

    @Override
    public String bodyToString() {
        StringBuilder degugString = new StringBuilder();
        degugString
                .append(" Body field-> ")
                .append(" messageId= ").append(messageId)
                .append(" sourceAddress= ").append(sourceAddress);

        return degugString.toString();
    }

}
