/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import ru.enterix.nettysmpplib.constants.CommandId;


/**
 *
 * @author SoftProfi
 */
public class BindReceiver extends BindRequest{

    public BindReceiver() {
	super(CommandId.BIND_RECEIVER);
    }

    
    @Override
    public boolean isTransmitter() {
       return false;
    }

    @Override
    public boolean isReceiver() {
       return true;
    }

    @Override
    public Response createResponse() {
        return  new BindReceiverResp();
    }

    
    
}
