
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import static ru.enterix.nettysmpplib.constants.SmLenField.SM_PASS_LEN;
import static ru.enterix.nettysmpplib.constants.SmLenField.SM_SYSID_LEN;
import static ru.enterix.nettysmpplib.constants.SmLenField.SM_SYSTYPE_LEN;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import static ru.enterix.nettysmpplib.pdus.ByteData.getCString;
import static ru.enterix.nettysmpplib.pdus.ByteData.writeToChannelBufferFromCString;

/**
 *
 * @author SoftProfi
 */
public abstract class BindRequest extends Request {

    public static final int FIXED_LEN_BYTES = 1;
    private String systemId;
    private String password;
    private String systemType;
    private byte interfaceVersion;
    private AddressRange esmeAddress = new AddressRange();

    public BindRequest(int id) {
        super(id);
    }

    public abstract boolean isTransmitter();

    public abstract boolean isReceiver();

    @Override
    public ChannelBuffer getBytesFromData() {
        ChannelBuffer addrBytes = esmeAddress.getBytesFromData();
        int maxLenBytes = (FIXED_LEN_BYTES + lenCString(systemId) + lenCString(password) + lenCString(systemType)
                + addrBytes.readableBytes());

        ChannelBuffer buffer = ChannelBuffers.buffer(maxLenBytes);

        writeToChannelBufferFromCString(buffer, systemId);
        writeToChannelBufferFromCString(buffer, password);
        writeToChannelBufferFromCString(buffer, systemType);
        buffer.writeByte(interfaceVersion);
        buffer.writeBytes(addrBytes);

        return buffer;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setSystemId(getCString(buffer));
        setPassword(getCString(buffer));
        setSystemType(getCString(buffer));
        setInterfaceVersion(buffer.readByte());
        esmeAddress.setDataFromBytes(buffer);
    }

    //<editor-fold defaultstate="collapsed" desc="Setters Fields">
    public void setSystemId(String systemId) {
        validationCString(SM_SYSID_LEN, systemId);
        this.systemId = systemId;
    }

    public void setPassword(String password) {
        validationCString(SM_PASS_LEN, password);
        this.password = password;
    }

    public void setSystemType(String systemType) {
        validationCString(SM_SYSTYPE_LEN, systemType);
        this.systemType = systemType;
    }

    public void setInterfaceVersion(byte interfaceVersion) {
        this.interfaceVersion = interfaceVersion;
    }

    public void setEsmeAddress(byte ton, byte npi, String address) {
        this.esmeAddress.set(ton, npi, address);
    }
  //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters Fields">
    public String getSystemId() {
        return systemId;
    }

    public String getPassword() {
        return password;
    }

    public String getSystemType() {
        return systemType;
    }

    public byte getInterfaceVersion() {
        return interfaceVersion;
    }

    public AddressRange getEsmeAddress() {
        return esmeAddress;
    }
    //</editor-fold>

    @Override
    public String bodyToString() {
        StringBuilder debugString = new StringBuilder();
        debugString.append(super.toString())
                .append(" Body field-> ")
                .append(" systemId= ").append(systemId)
                .append(" password= ").append(password)
                .append(" systemType= ").append(systemType)
                .append(" interfaceVersion= ").append(interfaceVersion)
                .append(" esmeAddress= ").append(esmeAddress);

        return debugString.toString();
    }

}
