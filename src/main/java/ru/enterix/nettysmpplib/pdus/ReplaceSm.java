/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.EncodeString;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;

/**
 *
 * @author SoftProfi
 */
public class ReplaceSm extends Request {

    private static final int FIXED_LEN_BYTES = 3;
    //<editor-fold defaultstate="collapsed" desc="Main Fields">
    private String messageId;
    private Address sourceAddress = new Address();
    private String scheduleDeliveryTime;
    private String validityPeriod;
    private byte registeredDelivery;
    private byte dataCoding = EncodeString.LATIN1_int; // не учитвается, так по спецификации
    private byte smDefaultMsgId;
    private short smLength;
    private String shortMessage = "";

    // private ShortMessage shortMessage = new ShortMessage(SmLenField.SM_MSG_LEN);
    //</editor-fold>
    public ReplaceSm() {
        super(CommandId.REPLACE_SM);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        ChannelBuffer sourceAddressBytes = sourceAddress.getBytesFromData();
        int maxLenBytes = (FIXED_LEN_BYTES + lenCString(messageId) + sourceAddressBytes.readableBytes()
                + lenCString(scheduleDeliveryTime) + lenCString(validityPeriod) + smLength);

        ChannelBuffer buff = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(buff, messageId);
        buff.writeBytes(sourceAddressBytes);
        writeToChannelBufferFromCString(buff, scheduleDeliveryTime);
        writeToChannelBufferFromCString(buff, validityPeriod);
        buff.writeByte(registeredDelivery);
        buff.writeByte(smDefaultMsgId);
        buff.writeByte(encodeUnsigned(smLength));
        buff.writeBytes(shortMessage.getBytes(EncodeString.getCharSet(dataCoding)));
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setMessageId(getCString(buffer));
        sourceAddress.setDataFromBytes(buffer);
        setScheduleDeliveryTime(getCString(buffer));
        setValidityPeriod(getCString(buffer));
        registeredDelivery = buffer.readByte();
        smDefaultMsgId = buffer.readByte();
        smLength = buffer.readUnsignedByte();
        ChannelBuffer readBytes = buffer.readBytes(smLength);
        shortMessage = new String(readBytes.array(), EncodeString.getCharSet(dataCoding));
    }

    @Override
    public Response createResponse() {
        return new ReplaceSmResp();
    }

    //<editor-fold defaultstate="collapsed" desc="Main Setters">
    public void setMessageId(String messageID) {
        validationCString(SmLenField.SM_MSGID_LEN, messageID);
        this.messageId = messageID;
    }

    public void setSourceAddress(byte ton, byte npi, String address) {
        sourceAddress.set(ton, npi, address);
    }

    private void setScheduleDeliveryTime(String scheduleDeliveryTime) {
        validationCString(SmLenField.SM_DATE_LEN, scheduleDeliveryTime);
        this.scheduleDeliveryTime = scheduleDeliveryTime;
    }

    private void setValidityPeriod(String validityPeriod) {
        validationCString(SmLenField.SM_DATE_LEN, scheduleDeliveryTime);
        this.validityPeriod = validityPeriod;
    }

    public void setDataCoding(byte dataCoding) {
        this.dataCoding = dataCoding;
    }

    public void setRegisteredDelivery(byte registeredDelivery) {
        this.registeredDelivery = registeredDelivery;
    }

    public void setSmDefaultMsgId(byte smDefaultMsgId) {
        this.smDefaultMsgId = smDefaultMsgId;
    }

    public void setShortMessage(String message) {
        byte[] temp = message.getBytes(EncodeString.getCharSet(dataCoding));
        validationString(0, SmLenField.SM_MSG_LEN, temp.length);
        shortMessage = message;
        smLength = (short) temp.length;
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Main getters">

    public String getMessageId() {
        return messageId;
    }

    public Address getSourceAddress() {
        return sourceAddress;
    }

    public String getScheduleDeliveryTime() {
        return scheduleDeliveryTime;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public byte getRegisteredDelivery() {
        return registeredDelivery;
    }

    public byte getDataCoding() {
        return dataCoding;
    }

    public byte getSmDefaultMsgId() {
        return smDefaultMsgId;
    }

    public short getSmLength() {
        return smLength;
    }

    public String getShortMessage() {
        return shortMessage;
    }
    //</editor-fold>

    @Override
    public String bodyToString() {
        StringBuilder debugString = new StringBuilder();
        debugString
                .append(" Body field-> ")
                .append(" messageId= ").append(messageId)
                .append(" sourceAddress= ").append(sourceAddress)
                .append(" scheduleDeliveryTime= ").append(scheduleDeliveryTime)
                .append(" validityPeriod= ").append(validityPeriod)
                .append(" registeredDelivery= ").append(registeredDelivery)
                .append(" smDefaultMsgId= ").append(smDefaultMsgId)
                .append(" smLength= ").append(smLength)
                .append(" shortMessage= ").append(shortMessage);

        return debugString.toString();
    }

}
