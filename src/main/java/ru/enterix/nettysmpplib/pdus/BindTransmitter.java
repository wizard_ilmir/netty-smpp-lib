/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import ru.enterix.nettysmpplib.constants.CommandId;

/**
 *
 * @author SoftProfi
 */
public class BindTransmitter extends BindRequest{

    public BindTransmitter() {
	super(CommandId.BIND_TRANSMITTER);
    }

    @Override
    public boolean isTransmitter() {
        return true;
    }

    @Override
    public boolean isReceiver() {
        return false;
    }

    @Override
    public Response createResponse() {
        return  new BindTransmitterResp();
    }
}
