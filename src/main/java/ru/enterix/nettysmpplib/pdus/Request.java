/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

/**
 *
 * @author SoftProfi
 */
public abstract class Request extends Pdu {

    public Request(int id) {
        super(id);
    }

    public Response createResponseAndSetNumber() {
        if (canResponse()) {
            Response response = createResponse();
            response.setNumber(getNumber());
            return response;
        } else {
            return null;
        }
    }

    public abstract Response createResponse();

    @Override
    public boolean canResponse() {
        return true;
    }

    @Override
    public final boolean isRequest() {
        return true;
    }

    @Override
    public final boolean isResponse() {
        return false;
    }

}
