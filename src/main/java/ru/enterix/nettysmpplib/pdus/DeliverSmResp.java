/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;

/**
 *
 * @author SoftProfi
 */
public class DeliverSmResp extends Response {

    private String messageId;

    public DeliverSmResp() {
        super(CommandId.DELIVER_SM_RESP);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        int maxLenBytes = lenCString(messageId);
        ChannelBuffer buff = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(buff, messageId);
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setMessageId(getCString(buffer));
    }

    public void setMessageId(String messageId) {
        validationCString(SmLenField.SM_MSG_LEN, messageId);
        this.messageId = messageId;
    }

    public String getMessageId() {
        return messageId;
    }

    @Override
    public String bodyToString() {
        StringBuilder debugString = new StringBuilder();
        debugString
                .append(" Body field-> ")
                .append(" messageId= ").append(messageId);

        return debugString.toString();
    }

}
