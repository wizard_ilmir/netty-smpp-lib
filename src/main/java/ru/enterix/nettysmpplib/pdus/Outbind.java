/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import static ru.enterix.nettysmpplib.pdus.ByteData.getCString;
import static ru.enterix.nettysmpplib.pdus.ByteData.writeToChannelBufferFromCString;

/**
 *
 * @author SoftProfi
 */
// SMCS сингнализирует ESME о создание запроса bind_receiver
public class Outbind
        extends Request {

    private String systemId;
    private String password;

    public Outbind() {
        super(CommandId.OUTBIND);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        int maxLenBytes = lenCString(systemId) + lenCString(password);

        ChannelBuffer result = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(result, systemId);
        writeToChannelBufferFromCString(result, password);

        return result;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setSystemId(getCString(buffer));
        setPassword(getCString(buffer));
    }

    @Override
    public Response createResponse() {
        return null;
    }

    @Override
    public boolean canResponse() {
        return false;
    }

    public void setSystemId(String systemId) {
        validationCString(SmLenField.SM_SYSID_LEN, systemId);
        this.systemId = systemId;
    }

    public void setPassword(String password) {
        validationCString(SmLenField.SM_PASS_LEN, password);
        this.password = password;
    }

    public String getSystemId() {
        return systemId;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String bodyToString() {
        StringBuilder debugString = new StringBuilder();
        debugString
                .append(" Body field-> ")
                .append(" systemId= ").append(systemId)
                .append(" password= ").append(password);

        return debugString.toString();

    }

}
