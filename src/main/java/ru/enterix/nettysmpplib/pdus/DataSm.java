/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import java.util.ArrayList;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import ru.enterix.nettysmpplib.constants.CommandId;
import ru.enterix.nettysmpplib.constants.EncodeString;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_ALERT_ON_MSG_DELIVERY;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_ATAG;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_ATAG_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_ATAG_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_CALLBACK_NUM_PRES_IND;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DEST_SUBADDR_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DISPLAY_TIME;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DST_ADDR_SUBUNIT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DST_BEAR_TYPE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DST_NW_TYPE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DST_PORT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_DST_TELE_ID;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_ITS_REPLY_TYPE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_ITS_SESSION_INFO;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_LANG_IND;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MORE_MSGS;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_PAYLOAD_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_STATE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MSG_WAIT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_MS_VALIDITY;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_NUM_MSGS;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_NW_ERR_CODE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_NW_ERR_CODE_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_NW_ERR_CODE_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_PAYLOAD_TYPE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_PRIV_IND;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_QOS_TIME_TO_LIVE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_RECP_MSG_ID;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_RECP_MSG_ID_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_RECP_MSG_ID_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_MSG_REF_NUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_SEG_SNUM;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SAR_TOT_SEG;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SET_DPF;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SMS_SIGNAL;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_ADDR_SUBUNIT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_BEAR_TYPE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_NW_TYPE;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_PORT;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR_MAX;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_SUBADDR_MIN;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_SRC_TELE_ID;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_USER_MSG_REF;
import static ru.enterix.nettysmpplib.constants.OptionalTag.OPT_PAR_USER_RESP_CODE;
import ru.enterix.nettysmpplib.constants.SmLenField;
import ru.enterix.nettysmpplib.exceptions.IntegerOutOfRangeException;
import ru.enterix.nettysmpplib.exceptions.InvalidLengthOptinalParametrException;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import static ru.enterix.nettysmpplib.pdus.ByteData.lenCString;
import ru.enterix.nettysmpplib.pdus.tlvs.TLV;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVByte;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVEmpty;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVInt;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVOctets;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVShort;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVString;
import ru.enterix.nettysmpplib.pdus.tlvs.TLVUByte;

/**
 *
 * @author SoftProfi
 */
public class DataSm extends Request {

    //<editor-fold defaultstate="collapsed" desc="Main Fields">
    private static final int FIXED_LEN_BYTES = 3;
    private static int COUNT_OPTIMAL_PARAM = 38;
    private String serviceType;
    private Address sourceAddress = new Address(SmLenField.SM_DATA_ADDR_LEN);
    private Address destAddress = new Address(SmLenField.SM_DATA_ADDR_LEN);
    private byte esmClass;
    private byte registeredDelivery;
    private byte dataCoding = EncodeString.LATIN1_int;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Others Fields">
    private TLVShort userMessageReference = new TLVShort(OPT_PAR_USER_MSG_REF);
    private TLVShort sourcePort = new TLVShort(OPT_PAR_SRC_PORT);
    private TLVByte sourceAddrSubunit = new TLVByte(OPT_PAR_SRC_ADDR_SUBUNIT);
    private TLVByte sourceNetworkType = new TLVByte(OPT_PAR_SRC_NW_TYPE);
    private TLVByte sourceBearerType = new TLVByte(OPT_PAR_SRC_BEAR_TYPE);
    private TLVByte sourceTelematicsId = new TLVByte(OPT_PAR_SRC_TELE_ID);
    private TLVShort destinationPort = new TLVShort(OPT_PAR_DST_PORT);
    private TLVByte destAddrSubunit = new TLVByte(OPT_PAR_DST_ADDR_SUBUNIT);
    private TLVByte destNetworkType = new TLVByte(OPT_PAR_DST_NW_TYPE);
    private TLVByte destBearerType = new TLVByte(OPT_PAR_DST_BEAR_TYPE);
    private TLVShort destTelematicsId = new TLVShort(OPT_PAR_DST_TELE_ID);
    private TLVShort sarMsgRefNum = new TLVShort(OPT_PAR_SAR_MSG_REF_NUM);
    private TLVUByte sarTotalSegments = new TLVUByte(OPT_PAR_SAR_TOT_SEG);
    private TLVUByte sarSegmentSeqnum = new TLVUByte(OPT_PAR_SAR_SEG_SNUM);
    private TLVByte moreMsgsToSend = new TLVByte(OPT_PAR_MORE_MSGS);
    private TLVInt qosTimeToLive = new TLVInt(OPT_PAR_QOS_TIME_TO_LIVE);
    private TLVByte payloadType = new TLVByte(OPT_PAR_PAYLOAD_TYPE);
    private TLVOctets messagePayload = new TLVOctets(OPT_PAR_MSG_PAYLOAD, OPT_PAR_MSG_PAYLOAD_MIN, OPT_PAR_MSG_PAYLOAD_MAX);
    private TLVByte setDpf = new TLVByte(OPT_PAR_SET_DPF);
    private TLVString receiptedMessageId = new TLVString(OPT_PAR_RECP_MSG_ID, OPT_PAR_RECP_MSG_ID_MIN, OPT_PAR_RECP_MSG_ID_MAX);
    private TLVByte messageState = new TLVByte(OPT_PAR_MSG_STATE);
    // exactly 3
    private TLVOctets networkErrorCode = new TLVOctets(OPT_PAR_NW_ERR_CODE, OPT_PAR_NW_ERR_CODE_MIN, OPT_PAR_NW_ERR_CODE_MAX);

    private TLVByte privacyIndicator = new TLVByte(OPT_PAR_PRIV_IND);
    private TLVOctets callbackNum = new TLVOctets(OPT_PAR_CALLBACK_NUM, OPT_PAR_CALLBACK_NUM_MIN, OPT_PAR_CALLBACK_NUM_MAX); // 4-19
    private TLVByte callbackNumPresInd = new TLVByte(OPT_PAR_CALLBACK_NUM_PRES_IND);
    private TLVOctets callbackNumAtag = new TLVOctets(OPT_PAR_CALLBACK_NUM_ATAG, OPT_PAR_CALLBACK_NUM_ATAG_MIN,
            OPT_PAR_CALLBACK_NUM_ATAG_MAX); // 1-65
    private TLVOctets sourceSubaddress = new TLVOctets(OPT_PAR_SRC_SUBADDR, OPT_PAR_SRC_SUBADDR_MIN, OPT_PAR_SRC_SUBADDR_MAX); // 2-23 
    private TLVOctets destSubaddress = new TLVOctets(OPT_PAR_DEST_SUBADDR, OPT_PAR_DEST_SUBADDR_MIN, OPT_PAR_DEST_SUBADDR_MAX);
    private TLVByte userResponseCode = new TLVByte(OPT_PAR_USER_RESP_CODE);
    private TLVByte displayTime = new TLVByte(OPT_PAR_DISPLAY_TIME);
    private TLVShort smsSignal = new TLVShort(OPT_PAR_SMS_SIGNAL);
    private TLVByte msValidity = new TLVByte(OPT_PAR_MS_VALIDITY);
    private TLVByte msMsgWaitFacilities = new TLVByte(OPT_PAR_MSG_WAIT); // bit mask
    private TLVByte numberOfMessages = new TLVByte(OPT_PAR_NUM_MSGS);
    private TLVEmpty alertOnMsgDelivery = new TLVEmpty(OPT_PAR_ALERT_ON_MSG_DELIVERY);
    private TLVByte languageIndicator = new TLVByte(OPT_PAR_LANG_IND);
    private TLVByte itsReplyType = new TLVByte(OPT_PAR_ITS_REPLY_TYPE);
    private TLVShort itsSessionInfo = new TLVShort(OPT_PAR_ITS_SESSION_INFO);

    //</editor-fold>
    public DataSm() {
        super(CommandId.DATA_SM);
        ArrayList<TLV> reg = new ArrayList<TLV>(COUNT_OPTIMAL_PARAM);
        reg.add(userMessageReference);
        reg.add(sourcePort);
        reg.add(sourceAddrSubunit);
        reg.add(sourceNetworkType);
        reg.add(sourceBearerType);
        reg.add(sourceTelematicsId);
        reg.add(destinationPort);
        reg.add(destAddrSubunit);
        reg.add(destNetworkType);
        reg.add(destBearerType);
        reg.add(destTelematicsId);
        reg.add(sarMsgRefNum);
        reg.add(sarTotalSegments);
        reg.add(sarSegmentSeqnum);
        reg.add(moreMsgsToSend);
        reg.add(qosTimeToLive);
        reg.add(payloadType);
        reg.add(messagePayload);
        reg.add(setDpf);
        reg.add(receiptedMessageId);
        reg.add(messageState);
        reg.add(networkErrorCode);
        reg.add(privacyIndicator);
        reg.add(callbackNum);
        reg.add(callbackNumPresInd);
        reg.add(callbackNumAtag);
        reg.add(sourceSubaddress);
        reg.add(destSubaddress);
        reg.add(userResponseCode);
        reg.add(displayTime);
        reg.add(smsSignal);
        reg.add(msValidity);
        reg.add(msMsgWaitFacilities);
        reg.add(numberOfMessages);
        reg.add(alertOnMsgDelivery);
        reg.add(languageIndicator);
        reg.add(itsReplyType);
        reg.add(itsSessionInfo);
        registerOptionalParam(reg);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        ChannelBuffer addressSourceBytes = sourceAddress.getBytesFromData();
        ChannelBuffer addressDestBytes = destAddress.getBytesFromData();
        int maxLenBytes = (FIXED_LEN_BYTES + lenCString(serviceType) + addressSourceBytes.readableBytes()
                + addressDestBytes.readableBytes());

        ChannelBuffer buff = ChannelBuffers.buffer(maxLenBytes);
        writeToChannelBufferFromCString(buff, serviceType);
        buff.writeBytes(sourceAddress.getBytesFromData());
        buff.writeBytes(destAddress.getBytesFromData());
        buff.writeByte(esmClass);
        buff.writeByte(registeredDelivery);
        buff.writeByte(dataCoding);
        return buff;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) throws ParserPduException {
        setServiceType(getCString(buffer));
        sourceAddress.setDataFromBytes(buffer);
        destAddress.setDataFromBytes(buffer);
        esmClass = buffer.readByte();
        registeredDelivery = buffer.readByte();
        dataCoding = buffer.readByte();
    }

    @Override
    public Response createResponse() {
        return new DataSmResp();
    }

    //<editor-fold defaultstate="collapsed" desc="Main Setters">
    public void setServiceType(String value) {
        validationCString(SmLenField.SM_SRVTYPE_LEN, value);
        serviceType = value;
    }

    public void setSourceAddress(byte ton, byte npi, String address) {
        sourceAddress.set(ton, npi, address);
    }

    public void setDestAddress(byte ton, byte npi, String address) {
        destAddress.set(ton, npi, address);
    }

    public void setEsmClass(byte value) {
        esmClass = value;
    }

    public void setRegisteredDelivery(byte value) {
        registeredDelivery = value;
    }

    public void setDataCoding(byte value) {
        dataCoding = value;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Main Getters">
    public String getServiceType() {
        return serviceType;
    }

    public Address getAddressSource() {
        return sourceAddress;
    }

    public Address getAddressDest() {
        return destAddress;
    }

    public byte getEsmClass() {
        return esmClass;
    }

    public byte getRegisteredDelivery() {
        return registeredDelivery;
    }

    public byte getDataCoding() {
        return dataCoding;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Has">
    public boolean hasUserMessageReference() {
        return userMessageReference.hasValue();
    }

    public boolean hasSourcePort() {
        return sourcePort.hasValue();
    }

    public boolean hasSourceAddrSubunit() {
        return sourceAddrSubunit.hasValue();
    }

    public boolean hasSourceNetworkType() {
        return sourceNetworkType.hasValue();
    }

    public boolean hasSourceBearerType() {
        return sourceBearerType.hasValue();
    }

    public boolean hasSourceTelematicsId() {
        return sourceTelematicsId.hasValue();
    }

    public boolean hasDestinationPort() {
        return destinationPort.hasValue();
    }

    public boolean hasDestAddrSubunit() {
        return destAddrSubunit.hasValue();
    }

    public boolean hasDestNetworkType() {
        return destNetworkType.hasValue();
    }

    public boolean hasDestBearerType() {
        return destBearerType.hasValue();
    }

    public boolean hasDestTelematicsId() {
        return destTelematicsId.hasValue();
    }

    public boolean hasSarMsgRefNum() {
        return sarMsgRefNum.hasValue();
    }

    public boolean hasSarTotalSegments() {
        return sarTotalSegments.hasValue();
    }

    public boolean hasSarSegmentSeqnum() {
        return sarSegmentSeqnum.hasValue();
    }

    public boolean hasMoreMsgsToSend() {
        return moreMsgsToSend.hasValue();
    }

    public boolean hasQosTimeToLive() {
        return qosTimeToLive.hasValue();
    }

    public boolean hasPayloadType() {
        return payloadType.hasValue();
    }

    public boolean hasMessagePayload() {
        return messagePayload.hasValue();
    }

    public boolean hasSetDpf() {
        return setDpf.hasValue();
    }

    public boolean hasReceiptedMessageId() {
        return receiptedMessageId.hasValue();
    }

    public boolean hasMessageState() {
        return messageState.hasValue();
    }

    public boolean hasNetworkErrorCode() {
        return networkErrorCode.hasValue();
    }

    public boolean hasPrivacyIndicator() {
        return privacyIndicator.hasValue();
    }

    public boolean hasCallbackNum() {
        return callbackNum.hasValue();
    }

    public boolean hasCallbackNumPresInd() {
        return callbackNumPresInd.hasValue();
    }

    public boolean hasCallbackNumAtag() {
        return callbackNumAtag.hasValue();
    }

    public boolean hasSourceSubaddress() {
        return sourceSubaddress.hasValue();
    }

    public boolean hasDestSubaddress() {
        return destSubaddress.hasValue();
    }

    public boolean hasUserResponseCode() {
        return userResponseCode.hasValue();
    }

    public boolean hasDisplayTime() {
        return displayTime.hasValue();
    }

    public boolean hasSmsSignal() {
        return smsSignal.hasValue();
    }

    public boolean hasMsValidity() {
        return msValidity.hasValue();
    }

    public boolean hasMsMsgWaitFacilities() {
        return msMsgWaitFacilities.hasValue();
    }

    public boolean hasNumberOfMessages() {
        return numberOfMessages.hasValue();
    }

    public boolean hasAlertOnMsgDelivery() {
        return alertOnMsgDelivery.hasValue();
    }

    public boolean hasLanguageIndicator() {
        return languageIndicator.hasValue();
    }

    public boolean hasItsReplyType() {
        return itsReplyType.hasValue();
    }

    public boolean hasItsSessionInfo() {
        return itsSessionInfo.hasValue();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Others Setters">
    public void setUserMessageReference(short value) {
        userMessageReference.setValue(value);
    }

    public void setSourcePort(short value) {
        sourcePort.setValue(value);
    }

    public void setSourceAddrSubunit(byte value) {
        sourceAddrSubunit.setValue(value);
    }

    public void setSourceNetworkType(byte value) {
        sourceNetworkType.setValue(value);
    }

    public void setSourceBearerType(byte value) {
        sourceBearerType.setValue(value);
    }

    public void setSourceTelematicsId(byte value) {
        sourceTelematicsId.setValue(value);
    }

    public void setDestinationPort(short value) {
        destinationPort.setValue(value);
    }

    public void setDestAddrSubunit(byte value) {
        destAddrSubunit.setValue(value);
    }

    public void setDestNetworkType(byte value) {
        destNetworkType.setValue(value);
    }

    public void setDestBearerType(byte value) {
        destBearerType.setValue(value);
    }

    public void setDestTelematicsId(short value) {
        destTelematicsId.setValue(value);
    }

    public void setSarMsgRefNum(short value) {
        sarMsgRefNum.setValue(value);
    }

    public void setSarTotalSegments(short value) {
        sarTotalSegments.setValue(value);
    }

    public void setSarSegmentSeqnum(short value)
            throws IntegerOutOfRangeException {
        sarSegmentSeqnum.setValue(value);
    }

    public void setMoreMsgsToSend(byte value) {
        moreMsgsToSend.setValue(value);
    }

    public void setQosTimeToLive(int value) {
        qosTimeToLive.setValue(value);
    }

    public void setPayloadType(byte value) {
        payloadType.setValue(value);
    }

    public void setMessagePayload(byte[] value) {
        messagePayload.setValue(value);
    }

    public void setSetDpf(byte value) {
        setDpf.setValue(value);
    }

    public void setReceiptedMessageId(String value) {
        receiptedMessageId.setValue(value);
    }

    public void setMessageState(byte value) {
        messageState.setValue(value);
    }

    public void setNetworkErrorCode(byte[] value)
            throws InvalidLengthOptinalParametrException {
        networkErrorCode.setValue(value);
    }

    public void setPrivacyIndicator(byte value) {
        privacyIndicator.setValue(value);
    }

    public void setCallbackNum(byte[] value) {
        callbackNum.setValue(value);
    }

    public void setCallbackNumPresInd(byte value) {
        callbackNumPresInd.setValue(value);
    }

    public void setCallbackNumAtag(byte[] value) {
        callbackNumAtag.setValue(value);
    }

    public void setSourceSubaddress(byte[] value) {
        sourceSubaddress.setValue(value);
    }

    public void setDestSubaddress(byte[] value) {
        destSubaddress.setValue(value);
    }

    public void setUserResponseCode(byte value) {
        userResponseCode.setValue(value);
    }

    public void setDisplayTime(byte value) {
        displayTime.setValue(value);
    }

    public void setSmsSignal(short value) {
        smsSignal.setValue(value);
    }

    public void setMsValidity(byte value) {
        msValidity.setValue(value);
    }

    public void setMsMsgWaitFacilities(byte value) {
        msMsgWaitFacilities.setValue(value);
    }

    public void setNumberOfMessages(byte value) {
        numberOfMessages.setValue(value);
    }

    public void setAlertOnMsgDelivery(boolean value) {
        alertOnMsgDelivery.setValue(value);
    }

    public void setLanguageIndicator(byte value) {
        languageIndicator.setValue(value);
    }

    public void setItsReplyType(byte value) {
        itsReplyType.setValue(value);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Others Getters">
    public void setItsSessionInfo(short value) {
        itsSessionInfo.setValue(value);
    }

    public short getUserMessageReference() {
        return userMessageReference.getValue();
    }

    public short getSourcePort() {
        return sourcePort.getValue();
    }

    public byte getSourceAddrSubunit() {
        return sourceAddrSubunit.getValue();
    }

    public byte getSourceNetworkType() {
        return sourceNetworkType.getValue();
    }

    public byte getSourceBearerType() {
        return sourceBearerType.getValue();
    }

    public byte getSourceTelematicsId() {
        return sourceTelematicsId.getValue();
    }

    public short getDestinationPort() {
        return destinationPort.getValue();
    }

    public byte getDestAddrSubunit() {
        return destAddrSubunit.getValue();
    }

    public byte getDestNetworkType() {
        return destNetworkType.getValue();
    }

    public byte getDestBearerType() {
        return destBearerType.getValue();
    }

    public short getDestTelematicsId() {
        return destTelematicsId.getValue();
    }

    public short getSarMsgRefNum() {
        return sarMsgRefNum.getValue();
    }

    public short getSarTotalSegments() {
        return sarTotalSegments.getValue();
    }

    public short getSarSegmentSeqnum() {
        return sarSegmentSeqnum.getValue();
    }

    public byte getMoreMsgsToSend() {
        return moreMsgsToSend.getValue();
    }

    public int getQosTimeToLive() {
        return qosTimeToLive.getValue();
    }

    public byte getPayloadType() {
        return payloadType.getValue();
    }

    public ChannelBuffer getMessagePayload() {
        return messagePayload.getValue();
    }

    public byte getSetDpf() {
        return setDpf.getValue();
    }

    public String getReceiptedMessageId() {
        return receiptedMessageId.getValue();
    }

    public byte getMessageState() {
        return messageState.getValue();
    }

    public ChannelBuffer getNetworkErrorCode() {
        return networkErrorCode.getValue();
    }

    public byte getPrivacyIndicator() {
        return privacyIndicator.getValue();
    }

    public ChannelBuffer callbackNum() {
        return callbackNum.getValue();
    }

    public byte getCallbackNumPresInd() {
        return callbackNumPresInd.getValue();
    }

    public ChannelBuffer getCallbackNumAtag() {
        return callbackNumAtag.getValue();
    }

    public ChannelBuffer getSourceSubaddress() {
        return sourceSubaddress.getValue();
    }

    public ChannelBuffer getDestSubaddress() {
        return destSubaddress.getValue();
    }

    public byte getUserResponseCode() {
        return userResponseCode.getValue();
    }

    public byte getDisplayTime() {
        return displayTime.getValue();
    }

    public short getSmsSignal() {
        return smsSignal.getValue();
    }

    public byte getMsValidity() {
        return msValidity.getValue();
    }

    public byte getMsMsgWaitFacilities() {
        return msMsgWaitFacilities.getValue();
    }

    public byte getNumberOfMessages() {
        return numberOfMessages.getValue();
    }

    public boolean getAlertOnMsgDelivery() {
        return alertOnMsgDelivery.getValue();
    }

    public byte getLanguageIndicator() {
        return languageIndicator.getValue();
    }

    public byte getItsReplyType() {
        return itsReplyType.getValue();
    }

    public short getItsSessionInfo() {
        return itsSessionInfo.getValue();
    }
    //</editor-fold>

}
