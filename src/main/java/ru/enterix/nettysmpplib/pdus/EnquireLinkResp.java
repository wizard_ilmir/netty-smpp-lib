/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.enterix.nettysmpplib.constants.CommandId;

/**
 *
 * @author SoftProfi
 */
public class EnquireLinkResp
        extends Response {

    public EnquireLinkResp() {
        super(CommandId.ENQUIRE_LINK_RESP);
    }

    @Override
    public ChannelBuffer getBytesFromData() {
        return null;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) {
    }

}
