/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import org.apache.log4j.LogMF;
import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import static ru.enterix.nettysmpplib.constants.CommandId.ALERT_NOTIFICATION;
import static ru.enterix.nettysmpplib.constants.CommandId.BIND_RECEIVER;
import static ru.enterix.nettysmpplib.constants.CommandId.BIND_RECEIVER_RESP;
import static ru.enterix.nettysmpplib.constants.CommandId.BIND_TRANSCEIVER;
import static ru.enterix.nettysmpplib.constants.CommandId.BIND_TRANSCEIVER_RESP;
import static ru.enterix.nettysmpplib.constants.CommandId.BIND_TRANSMITTER;
import static ru.enterix.nettysmpplib.constants.CommandId.BIND_TRANSMITTER_RESP;
import static ru.enterix.nettysmpplib.constants.CommandId.CANCEL_SM;
import static ru.enterix.nettysmpplib.constants.CommandId.CANCEL_SM_RESP;
import static ru.enterix.nettysmpplib.constants.CommandId.DATA_SM;
import static ru.enterix.nettysmpplib.constants.CommandId.DATA_SM_RESP;
import static ru.enterix.nettysmpplib.constants.CommandId.DELIVER_SM;
import static ru.enterix.nettysmpplib.constants.CommandId.DELIVER_SM_RESP;
import static ru.enterix.nettysmpplib.constants.CommandId.ENQUIRE_LINK;
import static ru.enterix.nettysmpplib.constants.CommandId.ENQUIRE_LINK_RESP;
import static ru.enterix.nettysmpplib.constants.CommandId.GENERIC_NACK;
import static ru.enterix.nettysmpplib.constants.CommandId.OUTBIND;
import static ru.enterix.nettysmpplib.constants.CommandId.QUERY_SM;
import static ru.enterix.nettysmpplib.constants.CommandId.QUERY_SM_RESP;
import static ru.enterix.nettysmpplib.constants.CommandId.REPLACE_SM;
import static ru.enterix.nettysmpplib.constants.CommandId.REPLACE_SM_RESP;
import static ru.enterix.nettysmpplib.constants.CommandId.SUBMIT_MULTI;
import static ru.enterix.nettysmpplib.constants.CommandId.SUBMIT_MULTI_RESP;
import static ru.enterix.nettysmpplib.constants.CommandId.SUBMIT_SM;
import static ru.enterix.nettysmpplib.constants.CommandId.SUBMIT_SM_RESP;
import static ru.enterix.nettysmpplib.constants.CommandId.UNBIND;
import static ru.enterix.nettysmpplib.constants.CommandId.UNBIND_RESP;
import ru.enterix.nettysmpplib.exceptions.NotEnoughDataInByteBufferException;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import ru.enterix.nettysmpplib.exceptions.UnknownCommandIdException;
import ru.enterix.nettysmpplib.pdus.tlvs.TLV;

/**
 *
 * @author SoftProfi
 */
public abstract class Pdu extends ByteData {

    private static final Logger LOG = Logger.getLogger(Pdu.class.getName());
    public static final int HEAD_BYTES_MAX_LEN = 16;  // c длиной
    private final int id;
    private int status;
    private int number;
    private List<TLV> listRegisteredOptiomalParam = new ArrayList<>();

    protected Pdu(int id) {
        this.id = id;
    }

    public static Pdu createPdu(byte[] packed) throws ParserPduException {

        ChannelBuffer buffer = ChannelBuffers.wrappedBuffer(packed);
        int id = buffer.readInt();
        int status = buffer.readInt();
        int number = buffer.readInt();
        Pdu newPdu = createPdu(id);
        if (newPdu == null) {
            UnknownCommandIdException ex = new UnknownCommandIdException();
            ex.setPduBuffer(buffer);
            ex.setUnknownId(number);
            throw ex;
        }
        newPdu.status = status;
        newPdu.number = number;

        parseBody(buffer, newPdu);

        return newPdu;
    }

    private static void parseBody(ChannelBuffer buffer, Pdu newPdu) throws ParserPduException {
        try {
            newPdu.setDataFromBytes(buffer);
            setOptionalParam(newPdu, buffer);
        } catch (IndexOutOfBoundsException ex) {
            NotEnoughDataInByteBufferException newEx = new NotEnoughDataInByteBufferException("", ex);
            newEx.setPduBuffer(buffer);
            throw newEx;
        }

    }

    private static void setOptionalParam(Pdu pdu, ChannelBuffer buffer) throws ParserPduException {
        short tag;
        TLV tlv;

        while (buffer.readableBytes() != 0) {
            tag = buffer.readShort();
            tlv = findOptionalParam(pdu.listRegisteredOptiomalParam, tag);
            if (tlv != null) {
                tlv.setDataFromBytes(buffer);
            } else {
                LogMF.warn(LOG, "!!!tlv not found tag={0}", tag);
                short len = buffer.readShort();
                buffer.readBytes((int) len);
            }
        }

    }

    private ChannelBuffer getBytesFromOptionalParam() {
        if (listRegisteredOptiomalParam.isEmpty()) {
            return null;
        }
        int lenOptimalParam = 0;
        HashSet<TLV> hasValueOptimalParam = new HashSet<TLV>(listRegisteredOptiomalParam.size(), 1.1f);

        for (TLV tlv : listRegisteredOptiomalParam) {
            if (tlv.hasValue()) {
                lenOptimalParam = lenOptimalParam + tlv.getLen() + TLV.TLV_HEAD_LEN_BYTES;
                if (!hasValueOptimalParam.add(tlv)) {
                    LOG.warn("!!!The parser found two or more identical optimal parameters.");
                }
            }
        }
        if (lenOptimalParam == 0) {
            return null;
        }
        ChannelBuffer bufferOptimalParam = ChannelBuffers.buffer(lenOptimalParam);

        for (TLV tlv : hasValueOptimalParam) {
            bufferOptimalParam.writeBytes(tlv.getBytesFromData());
        }

        return bufferOptimalParam;
    }

    public byte[] getBytesFromPdu() {
        ChannelBuffer bodyBytes = getBytesFromData();
        ChannelBuffer optimalParamBytes = getBytesFromOptionalParam();

        int bodyBytesLen = bodyBytes == null ? 0 : bodyBytes.readableBytes();
        int optimalParamBytesLen = optimalParamBytes == null ? 0 : optimalParamBytes.readableBytes();

        int pduBytesLen = HEAD_BYTES_MAX_LEN + bodyBytesLen + optimalParamBytesLen;
        ChannelBuffer pduBufBytes = ChannelBuffers.buffer(pduBytesLen);
        pduBufBytes.writeInt(pduBytesLen);
        pduBufBytes.writeInt(id);
        pduBufBytes.writeInt(status);
        pduBufBytes.writeInt(number);
        if (bodyBytesLen != 0) {
            pduBufBytes.writeBytes(bodyBytes);
        }
        if (optimalParamBytesLen != 0) {
            pduBufBytes.writeBytes(optimalParamBytes);
        }

        return pduBufBytes.array();

    }

    protected void registerOptionalParam(ArrayList<TLV> regParam) {
        listRegisteredOptiomalParam = regParam;
    }

    private static TLV findOptionalParam(List<TLV> listRegisteredOptionalParam, short tag) {
        TLV tlv;
        ListIterator<TLV> iter = listRegisteredOptionalParam.listIterator();
        while (iter.hasNext()) {
            if ((tlv = iter.next()).getTag() == tag) {
                return tlv;
            }
        }

        return null;
    }

    public boolean canResponse() {
        return false;
    }

    public abstract boolean isRequest();

    public abstract boolean isResponse();

    public void setStatus(int status) {
        this.status = status;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public int getStatus() {
        return status;
    }

    public int getNumber() {
        return number;
    }

    public String bodyToString() {
        return "";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Pdu)) {
            return false;
        }
        Pdu pdu = (Pdu) obj;
        return (pdu.id == id
                && pdu.number == number
                && pdu.status == status);
    }

    public static Pdu createPdu(int id) {
        Pdu pdu = null;
        switch (id) {

            case GENERIC_NACK:
                pdu = new GenericNack();
                break;
            case BIND_RECEIVER:
                pdu = new BindReceiver();
                break;
            case BIND_RECEIVER_RESP:
                pdu = new BindReceiverResp();
                break;
            case BIND_TRANSMITTER:
                pdu = new BindTransmitter();
                break;
            case BIND_TRANSMITTER_RESP:
                pdu = new BindTransmitterResp();
                break;
            case QUERY_SM:
                pdu = new QuerySm();
                break;
            case QUERY_SM_RESP:
                pdu = new QuerySmResp();
                break;
            case SUBMIT_SM:
                pdu = new SubmitSm();
                break;
            case SUBMIT_SM_RESP:
                pdu = new SubmitSmResp();
                break;
            case DELIVER_SM:
                pdu = new DeliverSm();
                break;
            case DELIVER_SM_RESP:
                pdu = new DeliverSmResp();
                break;
            case UNBIND:
                pdu = new Unbind();
                break;
            case UNBIND_RESP:
                pdu = new UnbindResp();
                break;
            case REPLACE_SM:
                pdu = new ReplaceSm();
                break;
            case REPLACE_SM_RESP:
                pdu = new ReplaceSmResp();
                break;
            case CANCEL_SM:
                pdu = new CancelSm();
                break;
            case CANCEL_SM_RESP:
                pdu = new CancelSmResp();
                break;
            case BIND_TRANSCEIVER:
                pdu = new BindTransceiver();
                break;
            case BIND_TRANSCEIVER_RESP:
                pdu = new BindTransceiverResp();
                break;
            case OUTBIND:
                pdu = new Outbind();
                break;
            case ENQUIRE_LINK:
                pdu = new EnquireLink();
                break;
            case ENQUIRE_LINK_RESP:
                pdu = new EnquireLinkResp();
                break;
            case SUBMIT_MULTI:
                pdu = new SubmitMulti();
                break;
            case SUBMIT_MULTI_RESP:
                pdu = new SubmitMultiResp();
                break;
            case ALERT_NOTIFICATION:
                pdu = new AlertNotification();
                break;
            case DATA_SM:
                pdu = new DataSm();
                break;
            case DATA_SM_RESP:
                pdu = new DataSmResp();
                break;
            default:
                return null;

        }

        return pdu;
    }

}
