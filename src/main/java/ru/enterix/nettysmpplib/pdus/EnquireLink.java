/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.pdus;

import org.jboss.netty.buffer.ChannelBuffer;
import ru.enterix.nettysmpplib.constants.CommandId;

/**
 *
 * @author SoftProfi
 */
public class EnquireLink extends Request {

    public EnquireLink() {
	super(CommandId.ENQUIRE_LINK);
    }

   
    @Override
    public ChannelBuffer getBytesFromData() {
        return  null;
    }

    @Override
    public void setDataFromBytes(ChannelBuffer buffer) {
        
    }

    @Override
    public Response createResponse() {
	return new EnquireLinkResp();
    }
    
}
