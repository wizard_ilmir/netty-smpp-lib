/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.demo.esme;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import ru.enterix.nettysmpplib.demo.DecoderPduHandler;
import ru.enterix.nettysmpplib.demo.EncoderPduHandler;
import ru.enterix.nettysmpplib.pdus.BindReceiver;

/**
 *
 * @author SoftProfi
 */
class NettySmppClient
{

    public static void main (String[] args) 
    {

        ChannelFactory factory = new NioClientSocketChannelFactory(
        Executors.newFixedThreadPool(1), //распределяет работу между потоками
        Executors.newFixedThreadPool(4)); //рабочие потоки

        ClientBootstrap bootstrap = new ClientBootstrap(factory); // класс для открытия соединения

        //у каждого соединения свой Pipeline
        //ChannelPipelineFactory служит для создания
        //Pipeline для каждого соединения
        bootstrap.setPipelineFactory(new ChannelPipelineFactory()
        {
            public ChannelPipeline getPipeline ()
            {
                return Channels.pipeline(new DecoderPduHandler(), new EncoderPduHandler(), new LogicClientSmppHandler());
            }
        });
        //подкючение к серверу
        ChannelFuture future = bootstrap.connect(new InetSocketAddress(11111));
        //ожидания того что соединение произошло и получение chanel для записи
        Channel channel = null;
        channel = future.getChannel();
        channel.write(new BindReceiver());
    }

}
