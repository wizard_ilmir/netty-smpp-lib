/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.demo;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.handler.codec.frame.FrameDecoder;
import ru.enterix.nettysmpplib.pdus.Pdu;

/**
 *
 * @author SoftProfi
 */
public class DecoderPduHandler
extends FrameDecoder
{

    private static final Logger LOG = Logger.getLogger(DecoderPduHandler.class.getName());

    @Override
    protected Object decode (ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer)
    throws Exception
    {
        if (buffer.readableBytes() < 4) {
            return null;
        }

        buffer.markReaderIndex();
        int length = buffer.readInt();

        if (length < 4) {
//            throw new InvalidLengthPduException("Error lenght pdu. length<4");
        }

        if (buffer.readableBytes() < length - 4) {
            buffer.resetReaderIndex();
            return null;
        }

        ChannelBuffer bytesForPdu = buffer.readBytes(length - 4);
        Pdu pdu = Pdu.createPdu(bytesForPdu.array());
        return pdu;
    }

    @Override
    public void exceptionCaught (ChannelHandlerContext ctx, ExceptionEvent e) throws Exception
    {

       /* if (e.getCause() instanceof UnknownCommandIdException
        || e.getCause() instanceof InvalidCommandLengthException) {
            LOG.trace("Exception when parsing:", e.getCause());
            Pdu generic_nack = PduFactory.getInstance(CommandId.GENERIC_NACK);
            SmppException ex = (SmppException) e.getCause();
            generic_nack.setNumber(ex.getNumber());
            generic_nack.setStatus(ex.getErrorCode());
            ctx.getChannel().write(generic_nack).addListener(ChannelFutureListener.CLOSE);
        } else if (e.getCause() instanceof ParserBodyException) {
            LOG.trace("Exception when parsing:", e.getCause());
            ParserBodyException ex = (ParserBodyException) e.getCause();
            if (ex.getPdu().isRequest() && ex.getPdu().canResponse()) {
                Pdu pduResponse = ((Request) ex.getPdu()).createResponse();
                pduResponse.setNumber(ex.getPdu().getNumber());
                pduResponse.setNumber(ex.getErrorCode());
                ctx.getChannel().write(pduResponse).addListener(ChannelFutureListener.CLOSE);
            }
        }*/
    }

}
