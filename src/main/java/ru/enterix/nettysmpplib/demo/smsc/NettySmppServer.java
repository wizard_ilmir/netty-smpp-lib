/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.demo.smsc;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.execution.OrderedMemoryAwareThreadPoolExecutor;
import ru.enterix.nettysmpplib.demo.DecoderPduHandler;
import ru.enterix.nettysmpplib.demo.EncoderPduHandler;

/**
 *
 * @author SoftProfi
 */
class NettySmppServer {

    public static void main(String[] args) {
	ExecutorService bossExec = new OrderedMemoryAwareThreadPoolExecutor(1, 400000000, 2000000000, 60, TimeUnit.SECONDS);
	ExecutorService ioExec = new OrderedMemoryAwareThreadPoolExecutor(4, 400000000, 2000000000, 60, TimeUnit.SECONDS);
	ServerBootstrap networkServer = new ServerBootstrap(new NioServerSocketChannelFactory(bossExec, ioExec, 4));

	networkServer.setOption("backlog", 500);
	networkServer.setOption("connectTimeoutMillis", 10000);

	networkServer.setPipelineFactory(new ChannelPipelineFactory() {
	    public ChannelPipeline getPipeline() {
		return Channels.pipeline(
			new DecoderPduHandler(),
			new EncoderPduHandler(),
			new LogicServerSmppHandler());
	    }
	});

	Channel channel = networkServer.bind(new InetSocketAddress(11111));
    }

}
