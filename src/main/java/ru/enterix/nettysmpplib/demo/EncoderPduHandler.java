/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.demo;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import ru.enterix.nettysmpplib.pdus.Pdu;

/**
 *
 * @author SoftProfi
 */
public class EncoderPduHandler
extends OneToOneEncoder
{

    private static final Logger LOG = Logger.getLogger(EncoderPduHandler.class.getName());

    @Override
    protected Object encode (ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception
    {
        Pdu pdu = (Pdu) msg;
        byte[] bytesFromPdu = pdu.getBytesFromPdu();
        ChannelBuffer buff = ChannelBuffers.buffer(bytesFromPdu.length + 4);
        buff.writeInt(bytesFromPdu.length + 4);
        buff.writeBytes(bytesFromPdu);
        return buff;
    }

}
