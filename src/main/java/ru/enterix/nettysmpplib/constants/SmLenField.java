/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.constants;

/**
 *
 * @author SoftProfi
 */
public class SmLenField {
    
    public static final int SM_CONNID_LEN        = 16;
    public static final int SM_MSG_LEN           = 254;
    public static final int SM_SYSID_LEN         = 16;
    public static final int SM_MSGID_LEN         = 64;
    public static final int SM_PASS_LEN          = 9;
    public static final int SM_DATE_LEN          = 17;
    public static final int SM_SRVTYPE_LEN       = 6;
    public static final int SM_SYSTYPE_LEN       = 13;
    public static final int SM_ADDR_LEN          = 21;
    public static final int SM_DATA_ADDR_LEN     = 65;
    public static final int SM_ADDR_RANGE_LEN    = 41;
    public static final int SM_TYPE_LEN          = 13;
    public static final int SM_DL_NAME_LEN       = 21;
    public static final int SM_PARAM_NAME_LEN    = 10;
    public static final int SM_PARAM_VALUE_LEN   = 10;
    public static final int SM_MAX_CNT_DEST_ADDR = 254;
    
    //типы pdu

}
