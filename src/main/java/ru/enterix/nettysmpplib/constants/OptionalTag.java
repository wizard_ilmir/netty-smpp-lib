/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.constants;

/**
 *
 * @author SoftProfi
 */
public class OptionalTag {

    public static final short OPT_PAR_MSG_WAIT        = 2;
    
    // Privacy Indicator
    public static final short OPT_PAR_PRIV_IND        = 0x0201;
    
    // Source Subaddress
    public static final short OPT_PAR_SRC_SUBADDR         = 0x0202;
    public static final int   OPT_PAR_SRC_SUBADDR_MIN     = 2;
    public static final int   OPT_PAR_SRC_SUBADDR_MAX     = 23;
    
    // Destination Subaddress
    public static final short OPT_PAR_DEST_SUBADDR        = 0x0203;
    public static final int   OPT_PAR_DEST_SUBADDR_MIN    = 2;
    public static final int   OPT_PAR_DEST_SUBADDR_MAX    = 23;
    
    // User Message Reference
    public static final short OPT_PAR_USER_MSG_REF        = 0x0204;
    
    // User Response Code
    public static final short OPT_PAR_USER_RESP_CODE      = 0x0205;
    
    // Language Indicator
    public static final short OPT_PAR_LANG_IND            = 0x020D;
    
    // Source Port
    public static final short OPT_PAR_SRC_PORT            = 0x020A;
    
    // Destination Port
    public static final short OPT_PAR_DST_PORT            = 0x020B;
    
    // Concat Msg Ref Num
    public static final short OPT_PAR_SAR_MSG_REF_NUM     = 0x020C;
    
    // Concat Total Segments
    public static final short OPT_PAR_SAR_TOT_SEG         = 0x020E;
    
    // Concat Segment Seqnums
    public static final short OPT_PAR_SAR_SEG_SNUM        = 0x020F;
    
    // SC Interface Version
    public static final short OPT_PAR_SC_IF_VER           = 0x0210;
    
    // Display Time
    public static final short OPT_PAR_DISPLAY_TIME        = 0x1201;
    
    // Validity Information
    public static final short OPT_PAR_MS_VALIDITY         = 0x1204;
    
    // DPF Result
    public static final short OPT_PAR_DPF_RES             = 0x0420;
    
    // Set DPF
    public static final short OPT_PAR_SET_DPF             = 0x0421;
    
    // MS Availability Status
    public static final short OPT_PAR_MS_AVAIL_STAT       = 0x0422;
    
    // Network Error Code
    public static final short OPT_PAR_NW_ERR_CODE         = 0x0423;
    public static final int OPT_PAR_NW_ERR_CODE_MIN     = 3;
    public static final int OPT_PAR_NW_ERR_CODE_MAX     = 3;
    
    // Extended Short Message has no size limit
    
    // Delivery Failure Reason
    public static final short OPT_PAR_DEL_FAIL_RSN        = 0x0425;
    
    // More Messages to Follow
    public static final short OPT_PAR_MORE_MSGS           = 0x0426;
    
    // Message State
    public static final short OPT_PAR_MSG_STATE           = 0x0427;
    
    // Callback Number
    public static final short OPT_PAR_CALLBACK_NUM        = 0x0381;
    public static final int OPT_PAR_CALLBACK_NUM_MIN    = 4;
    public static final int OPT_PAR_CALLBACK_NUM_MAX    = 19;
    
    // Callback Number Presentation  Indicator
    public static final short OPT_PAR_CALLBACK_NUM_PRES_IND       = 0x0302;
    
    // Callback Number Alphanumeric Tag
    public static final short OPT_PAR_CALLBACK_NUM_ATAG           = 0x0303;
    public static final int OPT_PAR_CALLBACK_NUM_ATAG_MIN       = 1;
    public static final int OPT_PAR_CALLBACK_NUM_ATAG_MAX       = 65;
    
    // Number of messages in Mailbox
    public static final short OPT_PAR_NUM_MSGS            = 0x0304;
    
    // SMS Received Alert
    public static final short OPT_PAR_SMS_SIGNAL          = 0x1203;
    
    // Message Delivery Alert
    public static final short OPT_PAR_ALERT_ON_MSG_DELIVERY       = 0x130C;
    
    // ITS Reply Type
    public static final short OPT_PAR_ITS_REPLY_TYPE      = 0x1380;
    
    // ITS Session Info
    public static final short OPT_PAR_ITS_SESSION_INFO            = 0x1383;
    
    // USSD Service Op
    public static final short OPT_PAR_USSD_SER_OP         = 0x0501;
    
    // Priority
    public static final int SM_NOPRIORITY               = 0;
    public static final int SM_PRIORITY                 = 1;

    // Registered delivery
    //   SMSC Delivery Receipt (bits 1 & 0)
    public static final byte SM_SMSC_RECEIPT_MASK            = 0x03;
    public static final byte SM_SMSC_RECEIPT_NOT_REQUESTED   = 0x00;
    public static final byte SM_SMSC_RECEIPT_REQUESTED       = 0x01;
    public static final byte SM_SMSC_RECEIPT_ON_FAILURE      = 0x02;
    //   SME originated acknowledgement (bits 3 & 2)
    public static final byte SM_SME_ACK_MASK               = 0x0c;
    public static final byte SM_SME_ACK_NOT_REQUESTED      = 0x00;
    public static final byte SM_SME_ACK_DELIVERY_REQUESTED = 0x04;
    public static final byte SM_SME_ACK_MANUAL_REQUESTED   = 0x08;
    public static final byte SM_SME_ACK_BOTH_REQUESTED     = 0x0c;
    //   Intermediate notification (bit 5)
    public static final byte SM_NOTIF_MASK                 = 0x010;
    public static final byte SM_NOTIF_NOT_REQUESTED        = 0x000;
    public static final byte SM_NOTIF_REQUESTED            = 0x010;
    
    // Replace if Present flag
    public static final int SM_NOREPLACE                = 0;
    public static final int SM_REPLACE                  = 1;
    
    
    // Destination flag
    public static final int SM_DEST_SME_ADDRESS         = 1;
    public static final int SM_DEST_DL_NAME             = 2;
    
    // Higher Layer Message Type
    public static final int SM_LAYER_WDP                = 0;
    public static final int SM_LAYER_WCMP               = 1;
    
    // Operation Class
    public static final int SM_OPCLASS_DATAGRAM         = 0;
    public static final int SM_OPCLASS_TRANSACTION      = 3;
    
    // Originating MSC Address
    public static final short OPT_PAR_ORIG_MSC_ADDR       = (short)0x8081;
    public static final int OPT_PAR_ORIG_MSC_ADDR_MIN   = 1;
    public static final int OPT_PAR_ORIG_MSC_ADDR_MAX   = 24;
    
    // Destination MSC Address
    public static final short OPT_PAR_DEST_MSC_ADDR       = (short)0x8082;
    public static final int OPT_PAR_DEST_MSC_ADDR_MIN   = 1;
    public static final int OPT_PAR_DEST_MSC_ADDR_MAX   = 24;
    
    // Unused Tag
    public static final int OPT_PAR_UNUSED              = 0xffff;
    
    // Destination Address Subunit
    public static final short OPT_PAR_DST_ADDR_SUBUNIT        = 0x0005;
    
    // Destination Network Type
    public static final short OPT_PAR_DST_NW_TYPE         = 0x0006;
    
    // Destination Bearer Type
    public static final short OPT_PAR_DST_BEAR_TYPE       = 0x0007;
    
    // Destination Telematics ID
    public static final short OPT_PAR_DST_TELE_ID         = 0x0008;
    
    // Source Address Subunit
    public static final short OPT_PAR_SRC_ADDR_SUBUNIT    = 0x000D;
    
    // Source Network Type
    public static final short OPT_PAR_SRC_NW_TYPE         = 0x000E;
    
    // Source Bearer Type
    public static final short OPT_PAR_SRC_BEAR_TYPE       = 0x000F;
    
    // Source Telematics ID 
    public static final short OPT_PAR_SRC_TELE_ID         = 0x0010;
    
    // QOS Time to Live
    public static final short OPT_PAR_QOS_TIME_TO_LIVE        = 0x0017;
    public static final int OPT_PAR_QOS_TIME_TO_LIVE_MIN    = 1;
    public static final int OPT_PAR_QOS_TIME_TO_LIVE_MAX    = 4;
    
    // Payload Type
    public static final short OPT_PAR_PAYLOAD_TYPE        = 0x0019;
    
    // Additional Status Info Text
    public static final short OPT_PAR_ADD_STAT_INFO       = 0x001D;
    public static final int OPT_PAR_ADD_STAT_INFO_MIN   = 1;
    public static final int OPT_PAR_ADD_STAT_INFO_MAX   = 256;
    
    // Receipted Message ID
    public static final short OPT_PAR_RECP_MSG_ID         = 0x001E;
    public static final int OPT_PAR_RECP_MSG_ID_MIN     = 1;
    public static final int OPT_PAR_RECP_MSG_ID_MAX     = 65;
    
    // Message Payload
    public static final short OPT_PAR_MSG_PAYLOAD         = 0x0424;
    public static final int OPT_PAR_MSG_PAYLOAD_MIN     = 1;
    public static final int OPT_PAR_MSG_PAYLOAD_MAX     = 1500;

}
