/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.constants;

import java.nio.charset.Charset;

/**
 *
 * @author SoftProfi
 */
public class EncodeString
{

    public static final String US_ASCII = "US-ASCII";
    public static final String LATIN1 = "ISO-8859-1";
    public static final String CYRLLIC = "ISO-8859-5";
    public static final String LATIN_HEBREW = "ISO-8859-8";
    public static final String UCS2 = "ISO-10646-UCS-2";
    public static final int US_ASCII_int = 0b00000001;
    public static final int LATIN1_int = 0b00000011;
    public static final int CYRLLIC_int = 0b00000110;
    public static final int LATIN_HEBREW_int = 0b00000111;
    public static final int UCS2_int = 0b00001000;

    public static Charset getCharSet (int encoding)
    {
        String encodingString;
        switch (encoding) {
            case EncodeString.US_ASCII_int:
                encodingString = EncodeString.US_ASCII;
                break;
            case EncodeString.LATIN1_int:
                encodingString = EncodeString.LATIN1;
                break;
            case EncodeString.LATIN_HEBREW_int:
                encodingString = EncodeString.LATIN_HEBREW;
                break;
            case EncodeString.CYRLLIC_int:
                encodingString = EncodeString.CYRLLIC;
                break;
            case EncodeString.UCS2_int:
                encodingString = EncodeString.UCS2;
                break;
            default:
                encodingString = EncodeString.LATIN1;
        }
        Charset result = Charset.forName(encodingString);
        return result;
    }

    public static String toString (int encoding)
    {
        String result;
        switch (encoding) {
            case EncodeString.US_ASCII_int:
                result = EncodeString.US_ASCII;
                break;
            case EncodeString.LATIN1_int:
                result = EncodeString.LATIN1;
                break;
            case EncodeString.LATIN_HEBREW_int:
                result = EncodeString.LATIN_HEBREW;
                break;
            case EncodeString.CYRLLIC_int:
                result = EncodeString.CYRLLIC;
                break;
            case EncodeString.UCS2_int:
                result = EncodeString.UCS2;
                break;
            default:
                result = null;
        }
        return result;
    }

    public static int toInt (String name)
    {
        int result = 0;
        switch (name) {
            case EncodeString.US_ASCII:
                result = EncodeString.US_ASCII_int;
                break;
            case EncodeString.LATIN1:
                result = EncodeString.LATIN1_int;
                break;
            case EncodeString.LATIN_HEBREW:
                result = EncodeString.LATIN_HEBREW_int;
                break;
            case EncodeString.CYRLLIC:
                result = EncodeString.CYRLLIC_int;
                break;
            case EncodeString.UCS2:
                result = EncodeString.UCS2_int;
                break;
            default:
                result = 0;
        }
        return result;
    }

}
