/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.exceptions;

/**
 *
 * @author SoftProfi
 */
public class UnknownCommandIdException extends ParserPduException {

    private int unknownId;

    public UnknownCommandIdException() {
    }

    public void setUnknownId(int unknownId) {
        this.unknownId = unknownId;
    }

    public int getUnknownId() {
        return unknownId;
    }

}
