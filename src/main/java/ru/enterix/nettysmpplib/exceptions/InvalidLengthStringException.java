/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.exceptions;

/**
 *
 * @author ILMIR
 */
public class InvalidLengthStringException extends RuntimePduException {

    private int min;
    private int max;
    private int sourceLength;
    private String srt = "";

    public InvalidLengthStringException() {
    }

    public InvalidLengthStringException(String string) {
        super(string);
    }

    public InvalidLengthStringException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public InvalidLengthStringException(Throwable thrwbl) {
        super(thrwbl);
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setSourceLength(int sourceLength) {
        this.sourceLength = sourceLength;
    }

    public void setSrt(String srt) {
        this.srt = srt;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public int getSourceLength() {
        return sourceLength;
    }

    public String getSrt() {
        return srt;
    }

}
