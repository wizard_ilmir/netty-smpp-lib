/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.exceptions;

/**
 *
 * @author ILMIR
 */
public class RuntimePduException extends RuntimeException {

    public RuntimePduException() {
    }

    public RuntimePduException(String string) {
        super(string);
    }

    public RuntimePduException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public RuntimePduException(Throwable thrwbl) {
        super(thrwbl);
    }

}
