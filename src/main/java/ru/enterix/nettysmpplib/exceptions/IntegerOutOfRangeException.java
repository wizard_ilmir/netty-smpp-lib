/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.exceptions;

/**
 *
 * @author SoftProfi
 */
public class IntegerOutOfRangeException extends RuntimePduException {

    private int minRange;
    private int maxRange;
    private int sourceRange;

    public IntegerOutOfRangeException() {
    }

    public IntegerOutOfRangeException(String message) {
        super(message);
    }

    public IntegerOutOfRangeException(String message, Throwable thrwbl) {
        super(message, thrwbl);
    }

    public IntegerOutOfRangeException(Throwable thrwbl) {
        super(thrwbl);
    }

    public void setMinRange(int minRange) {
        this.minRange = minRange;
    }

    public void setMaxRange(int maxRange) {
        this.maxRange = maxRange;
    }

    public void setSourceRange(int sourceLength) {
        this.sourceRange = sourceLength;
    }

    public int getMinRange() {
        return minRange;
    }

    public int getMaxRange() {
        return maxRange;
    }

    public int getSourceRange() {
        return sourceRange;
    }


}
