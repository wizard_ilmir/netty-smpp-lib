/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.exceptions;

import org.jboss.netty.buffer.ChannelBuffer;

/**
 *
 * @author SoftProfi
 */
public class ParserPduException extends Exception {

    private ChannelBuffer pduBuffer;

    public ParserPduException() {
    }

    public ParserPduException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public ParserPduException(Throwable thrwbl) {
        super(thrwbl);
    }

    public ParserPduException(String string) {
        super(string);
    }

    public ParserPduException(ChannelBuffer pduBuffer) {
        this.pduBuffer = pduBuffer;
    }
    
    public void setPduBuffer(ChannelBuffer pduBuffer) {
        this.pduBuffer = pduBuffer;
    }

    public ChannelBuffer getPduBuffer() {
        return pduBuffer;
    }

}
