/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.exceptions;

import org.jboss.netty.buffer.ChannelBuffer;

/**
 *
 * @author SoftProfi
 */
public class TerminatingZeroNotFoundException extends ParserPduException {

    private ChannelBuffer stringBuffer;

    public TerminatingZeroNotFoundException() {
    }

    public TerminatingZeroNotFoundException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public TerminatingZeroNotFoundException(Throwable thrwbl) {
        super(thrwbl);
    }

    public TerminatingZeroNotFoundException(String string) {
        super(string);
    }

    public void setStringBuffer(ChannelBuffer stringBuffer) {
        this.stringBuffer = stringBuffer;
    }

    public ChannelBuffer getStringBuffer() {
        return stringBuffer;
    }

}
