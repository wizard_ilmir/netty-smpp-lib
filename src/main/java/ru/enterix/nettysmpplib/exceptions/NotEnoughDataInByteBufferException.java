/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.nettysmpplib.exceptions;

/**
 *
 * @author SoftProfi
 */
public class NotEnoughDataInByteBufferException extends ParserPduException {

    public NotEnoughDataInByteBufferException() {
    }

    public NotEnoughDataInByteBufferException(Throwable t) {
        super(t);
    }

    public NotEnoughDataInByteBufferException(String message, Throwable t) {
        super(message, t);
    }

}
